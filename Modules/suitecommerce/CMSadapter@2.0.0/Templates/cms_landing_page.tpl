{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="cms-landing-page-row">

	<div class="cms-landing-page-row-full-col" data-cms-area="cms-landing-page-placeholder-page-type" data-cms-area-filters="page_type"></div>
	<div class="cms-landing-page-row-full-col" data-cms-area="cms-landing-page-placeholder-path1" data-cms-area-filters="path"></div>
	<section class="facet-page-banner-content">
		<div class="facet-content-box" data-cms-area="content_banner_3a" data-cms-area-filters="path"></div>
		<div class="facet-content-box" data-cms-area="content_banner_3b" data-cms-area-filters="path"></div>
		<div class="facet-content-box" data-cms-area="content_banner_3c" data-cms-area-filters="path"></div>
	</section>
	<div class="cms-landing-page-row-full-col" data-cms-area="cms-landing-page-placeholder-path" data-cms-area-filters="path"></div>
</div>

{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="facets-item-cell-grid" data-type="item" data-item-id="{{itemId}}" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/Product">
	<meta itemprop="url" content="{{url}}"/>

	<div class="facets-item-cell-grid-image-wrapper">
		<a class="facets-item-cell-grid-link-image" href="{{url}}">
		<!-- {{imageUrl}} -->
			<img class="facets-item-cell-grid-image" src="{{resizeImage thumbnail.url 'thumbnail'}}" alt="{{thumbnail.altimagetext}}" itemprop="image"/>
			{{#if newItem}}
				<img src="/img/search/flag-new.png" class="search-image-overlay">
			{{/if}}
			{{#if saleItem}}
				<img src="/img/search/flag-sale.png" class="search-image-overlay">
			{{/if}}
			{{#if plusItem}}
				<img src="/img/search/flag-plus.png" class="search-image-overlay">
			{{/if}}
			{{#if overstockItem}}
				<img src="/img/search/flag-overstock.png" class="search-image-overlay">
			{{/if}}
			{{#if freeshipItem}}
				<img src="/img/search/flag-freeshipping.png" class="search-image-overlay">
			{{/if}}
			{{#if usedItem}}
				<img src="/img/search/flag-used.png" class="search-image-overlay">
			{{/if}}
		</a>

	</div>

	<div class="facets-item-cell-grid-details">
		<a class="facets-item-cell-grid-title" href="{{url}}">
			<h2><span itemprop="name">{{name}}</span></h2>
		</a>
		<a class="facets-item-cell-grid-sku" href="{{url}}">
			<span itemprop="sku">{{sku}}</span>
		</a>

		{{#unless noPrice}}
		<div class="facets-item-cell-grid-price" data-view="ItemViews.Price">
		</div>
		{{/unless}}

		{{#if showRating}}
		<div class="facets-item-cell-grid-rating" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" data-view="GlobalViews.StarRating">
		</div>
		{{/if}}

		<div style="display:none"data-view="ItemDetails.Options"></div>
		<div class="facets-item-cell-grid-stock">
			<div data-view="ItemViews.Stock"></div>
		</div>
		{{#if canAddToCart}}
			{{#unless noPrice}}
			<form class="facets-item-cell-grid-add-to-cart" data-toggle="add-to-cart">
				<input class="facets-item-cell-grid-add-to-cart-itemid" type="hidden" value="{{itemId}}" name="item_id"/>
				<input name="quantity" class="facets-item-cell-grid-add-to-cart-quantity" type="number" min="1" value="{{minQuantity}}"/>
				<input type="submit" class="facets-item-cell-grid-add-to-cart-button" value="{{translate 'Add to Cart'}}"/>
			</form>
			{{else}}
			<div>Contact us for details</div>
			{{/unless}}
		{{/if}}

	</div>
</div>
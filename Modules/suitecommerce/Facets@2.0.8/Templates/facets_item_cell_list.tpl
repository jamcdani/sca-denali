{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="facets-item-cell-list" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
	<div class="facets-item-cell-list-left">
		<div class="facets-item-cell-list-image-wrapper">
			<a class="facets-item-cell-list-link-image" href="{{url}}">
			<img class="facets-item-cell-list-image" src="{{resizeImage thumbnail.url 'thumbnail'}}" alt="{{thumbnail.altimagetext}}" itemprop="image"/>
			{{#if newItem}}
				<img src="/img/search/flag-new.png" class="search-image-overlay">
			{{/if}}
			{{#if saleItem}}
				<img src="/img/search/flag-sale.png" class="search-image-overlay">
			{{/if}}
			{{#if plusItem}}
				<img src="/img/search/flag-plus.png" class="search-image-overlay">
			{{/if}}
			{{#if overstockItem}}
				<img src="/img/search/flag-overstock.png" class="search-image-overlay">
			{{/if}}
			{{#if freeshipItem}}
				<img src="/img/search/flag-freeshipping.png" class="search-image-overlay">
			{{/if}}
			{{#if usedItem}}
				<img src="/img/search/flag-used.png" class="search-image-overlay">
			{{/if}}
		</a>

		</div>
	</div>
	<div class="facets-item-cell-list-right">
		<meta itemprop="url" content="{{url}}">
		<h2 class="facets-item-cell-list-title">
			{{#if itemIsNavigable}}
				<a class="facets-item-cell-list-name" href='{{url}}'>
					<span itemprop="name">
						{{name}}
					</span>
				</a>
			{{else}}
				<span itemprop="name">
					{{name}}
				</span>
			{{/if}}
		</h2>

		{{#unless noPrice}}		
			<div class="facets-item-cell-list-price">
				<div data-view="ItemViews.Price"></div>
			</div>
		{{/unless}}

		{{#if showRating}}
		<div class="facets-item-cell-list-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"  data-view="GlobalViews.StarRating">
		{{/if}}

		<div data-view="ItemDetails.Options"></div>

		{{#unless noPrice}}
			{{#if canAddToCart}}
				<form class="facets-item-cell-list-add-to-cart" data-toggle="add-to-cart">
					<input class="facets-item-cell-list-add-to-cart-itemid" type="hidden" value="{{itemId}}" name="item_id">
					<input class="facets-item-cell-list-add-to-cart-quantity" name="quantity" type="number" min="1" value="{{minQuantity}}">
					<input class="facets-item-cell-list-add-to-cart-button" type="submit" value="{{translate 'Add to Cart'}}">
				</form>
			{{/if}}
		{{else}}
			<div>Contact us for details</div>
		{{/unless}}
		<div class="facets-item-cell-list-stock">
			<div data-view="ItemViews.Stock"></div>
		</div>
	</div>
</div>
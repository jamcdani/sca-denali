{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="facets-item-cell-table" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
	<meta itemprop="url" content="{{url}}">
	<div class="facets-item-cell-table-image-wrapper">
		<a class="facets-item-cell-table-link-image" href="{{url}}">
			<img class="facets-item-cell-table-image" src="{{resizeImage thumbnail.url 'main'}}" alt="{{thumbnail.altimagetext}}" itemprop="image">
			{{#if newItem}}
				<img src="/img/search/flag-new.png" class="search-image-overlay">
			{{/if}}
			{{#if saleItem}}
				<img src="/img/search/flag-sale.png" class="search-image-overlay">
			{{/if}}
			{{#if plusItem}}
				<img src="/img/search/flag-plus.png" class="search-image-overlay">
			{{/if}}
			{{#if overstockItem}}
				<img src="/img/search/flag-overstock.png" class="search-image-overlay">
			{{/if}}
			{{#if freeshipItem}}
				<img src="/img/search/flag-freeshipping.png" class="search-image-overlay">
			{{/if}}
			{{#if usedItem}}
				<img src="/img/search/flag-used.png" class="search-image-overlay">
			{{/if}}
		
		</a>
		{{#if isEnvironmentBrowser}}
			<div class="facets-item-cell-table-quick-view-wrapper">
				<a href="{{url}}" class="facets-item-cell-table-quick-view-link" data-toggle="show-in-modal">
				<i class="facets-item-cell-table-quick-view-icon"></i>
				{{translate 'Quick View'}}
			</a>
			</div>
		{{/if}}
	</div>
	<h2 class="facets-item-cell-table-title">
		<a href="{{url}}">
			<h2><span itemprop="name">
				{{name}}
			</span></h2>
		</a>
	</h2>
	
	{{#unless noPrice}}
		<div class="facets-item-cell-table-price">
			<div data-view="ItemViews.Price"></div>
		</div>
	{{/unless}}
	
	{{#if showRating}}
	<div class="facets-item-cell-table-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"  data-view="GlobalViews.StarRating">
	</div>
	{{/if}}
	<div data-view="ItemDetails.Options"></div>
	
	{{#unless noPrice}}
	{{#if canAddToCart}}
		<form class="facets-item-cell-table-add-to-cart" data-toggle="add-to-cart">
			<input class="facets-item-cell-table-add-to-cart-itemid" type="hidden" value="{{itemId}}" name="item_id">
			<input class="facets-item-cell-table-add-to-cart-quantity" name="quantity" type="number" min="1" value="{{minQuantity}}">
			<input class="facets-item-cell-table-add-to-cart-button" type="submit" value="Add to Cart">
		</form>
	{{/if}}
				{{else}}
			<div>Contact us for details</div>
	{{/unless}}
	<div class="facets-item-cell-table-stock">
		<div data-view="ItemViews.Stock"></div>
	</div>
</div>

{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div data-view="Global.BackToTop"></div>
<div class="footer-content">

	<section class="home-cms-page-main" data-cms-area="footer_main" data-cms-area-filters="global"></section>

	<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-footer-table" data-cms-area="footer_1" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_2" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_3" data-cms-area-filters="global"></div>
		
		<div class="home-cms-page-footer-table" data-cms-area="footer_4" data-cms-area-filters="global"></div>
	</section>
		
	<section class="home-cms-page-main" data-cms-area="footer_main_1" data-cms-area-filters="global"></section>

	<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-footer-table" data-cms-area="footer_1a" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_2a" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_3a" data-cms-area-filters="global"></div>
		
		<div class="home-cms-page-footer-table" data-cms-area="footer_4a" data-cms-area-filters="global"></div>
	</section>
		<section class="home-cms-page-main" data-cms-area="footer_main_2" data-cms-area-filters="global"></section>

	<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-footer-table" data-cms-area="footer_1b" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_2b" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_3b" data-cms-area-filters="global"></div>
		
		<div class="home-cms-page-footer-table" data-cms-area="footer_4b" data-cms-area-filters="global"></div>
	</section>
	
		<section class="home-cms-page-main" data-cms-area="footer_main_3" data-cms-area-filters="global"></section>

	<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-footer-table" data-cms-area="footer_1c" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_2c" data-cms-area-filters="global"></div>

		<div class="home-cms-page-footer-table" data-cms-area="footer_3c" data-cms-area-filters="global"></div>
		
		<div class="home-cms-page-footer-table" data-cms-area="footer_4c" data-cms-area-filters="global"></div>
	</section>
	
	<section class="home-cms-page-main" data-cms-area="footer_main_4" data-cms-area-filters="global"></section>
		
		<div class="footer-content-logos">
		<a target="_blank" href="http://www.bbb.org/alaskaoregonwesternwashington/business-reviews/screen-printing-equipment-and-supplies/ryonet-corporation-in-vancouver-wa-22024393/"><img class = "bbb-img" src="/img/bbb.png"></a>
	</div>
	<div class="footer-content-logos">
		<img src="/img/visa.png"><img src="/img/master.png"><img src="/img/discover.png"><img src="/img/american.png"><img src="/img/paypal.png">
	</div>
	<div class="footer-content-copyright">
		{{translate '&copy; 2004-'}}{{curyear}} Ryonet Corporation<br>Powering The Print. All rights reserved.
	</div>
</div>

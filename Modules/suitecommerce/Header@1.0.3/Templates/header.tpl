{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
	
<div class="header-message" data-type="message-placeholder"></div>

<div class="header-main-wrapper">

	<nav class="header-main-nav">
	
	<section class="home-cms-page-banner-bottom-content">
	
	<div id="banner-header-top" class="home-cms-page-banner-bottom" data-cms-area="header_banner_top_1" data-cms-area-filters="global"></div>
	
	<div id="banner-header-top" class="home-cms-page-banner-bottom" data-cms-area="header_banner_top" data-cms-area-filters="global"></div>
	
	<div id="banner-header-top" class="home-cms-page-banner-bottom" data-cms-area="header_banner_top_3" data-cms-area-filters="global"></div>
	</section>
	

		
		<div class="header-sidebar-toggle-wrapper">
			<button class="header-sidebar-toggle" data-action="header-sidebar-show">
				<i class="header-sidebar-toggle-icon"></i>
			</button>
		</div>

		<div class="header-content row">
			<div class="header-logo-wrapper row-segment">
				<div data-view="Header.Logo"></div>
			</div>
			<div class="row-segment nav-search-row" data-view="SiteSearch"></div>

			
			<div class="row-segment header-right-menu">

				<div class="header-menu-profile" data-view="Header.Profile">
				</div>

				{{#if showLanguagesOrCurrencies}}
				<div class="header-menu-settings">
					<a href="#" class="header-menu-settings-link" data-toggle="dropdown" title="{{translate 'Settings'}}">
						<i class="header-menu-settings-icon"></i>
						<i class="header-menu-settings-carret"></i>
					</a>
					<div class="header-menu-settings-dropdown">
						<h5 class="header-menu-settings-dropdown-title">{{translate 'Site Settings'}}</h5>
						{{#if showLanguages}}
							<div data-view="Global.HostSelector"></div>
						{{/if}}
						{{#if showCurrencies}}
							<div data-view="Global.CurrencySelector"></div>
						{{/if}}
					</div>
				</div>
				{{/if}}

				<div class="header-menu-cart">
					<div class="header-menu-cart-dropdown" >
						<div data-view="Header.MiniCart"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="banner-header-bottom" class="content-banner banner-header-bottom" data-cms-area="header_banner_bottom" data-cms-area-filters="global"></div>
	</nav>
</div>

<div class="header-sidebar-overlay" data-action="header-sidebar-hide"></div>
<div id="menu-wrapper" class="header-secondary-wrapper" data-view="Header.Menu" data-phone-template="header_sidebar" data-tablet-template="header_sidebar">
</div>

<script>
var host = window.location.host;
if(host.indexOf('checkout')==0){
	document.getElementById('menu-wrapper').setAttribute('style','display:none');
}
</script>
{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home-cms">
	<!-- Top header banner -->

	<!--Main Banner -->
	<section class="home-cms-page-main-abs" data-cms-area="home_main-over" data-cms-area-filters="path"></section>
	<section class="home-cms-page-main" data-cms-area="home_main" data-cms-area-filters="path"></section>

	<!--Banner / Promo--> 
	
	<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_1z" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_2z" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_3z" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom-main" data-cms-area="item_list_banner_bottomz" data-cms-area-filters="path"></div>
	</section>
	<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_1" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_2" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_3" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom-main" data-cms-area="item_list_banner_bottom" data-cms-area-filters="path"></div>
	</section>
		<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_1a" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_2a" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_3a" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom-main" data-cms-area="item_list_banner_bottom-a" data-cms-area-filters="path"></div>
	</section>
		<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_1b" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_2b" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_3b" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom-main" data-cms-area="item_list_banner_bottom-b" data-cms-area-filters="path"></div>
	</section>
		<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_1c" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_2c" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_3c" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom-main" data-cms-area="item_list_banner_bottom-c" data-cms-area-filters="path"></div>
	</section>
		<section class="home-cms-page-banner-bottom-content">
		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_1d" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_2d" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom" data-cms-area="home_banner_3d" data-cms-area-filters="path"></div>

		<div class="home-cms-page-banner-bottom-main" data-cms-area="item_list_banner_bottom-d" data-cms-area-filters="path"></div>
	</section>

	<!--Merchandising zone-->
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom" data-cms-area-filters="path"></section>
</div>
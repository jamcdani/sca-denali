{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<meta property="og:image" content="{{mainItemImage}}"/>
<!-- VIEW SHIPPING MAP (POPUP) -->
	<!-- Needs script at the end of page to operate -->
    
    <div onclick="popup('popUpDiv')" class = "blanket" id="blanket" style="display:none;">
    </div>
	<div class = "popUpDiv" id="popUpDiv" style="display:none;">
    
    	<a href="#" onclick="popup('popUpDiv')" >Close</a>
 
    	<div style="margin:0px; text-align:center"><strong>Shipping Transit Time</strong></div>
    	<div>
    		<img style="width: 100%" src="{{map}}">
		</div>						
		<div>		 
			
			<div style="text-align:center; margin:auto;">
		
				{{#if international}}
					<div style="margin-top:-15px; margin-bottom:15px;">
						<div>
							<img src="/img/international.jpg"> <strong>Available to ship internationally</strong>
						</div>
					</div>
				{{/if}}
				
		
				<!-- SHIPPING LOCATION & QUANTITY TABLE -->
					{{#if sameday}}
						<div style="width:100%;">
							<div style="margin-top:5px;">Available to ship <strong>Same Day</strong> from:</div>
							<table align="center" width="100%">
								<tr>
									<td class="mobile-hide">State:</td>
									{{#each qtylocs}}
									<td>{{location}}</td>
									{{/each}}
								</tr>
								<tr>
									<td class="mobile-hide">Qty:</td>
									{{#each qtylocs}}
									<td>{{quantity}}</td>
									{{/each}}
								</tr>
							</table>
						</div>
					{{/if}}
					
					{{#unless sameday}}
					Available to ship in <strong>{{leadtime}}</strong> from:
						<div style="margin-top:5px;>
							
							<table align="center" width="100%">
								<tr>
									{{#each qtylocs}}
									<td>{{location}}</td> 
									{{/each}}
								</tr>
							</table>
						</div>
					{{/unless}}
					
				<div class="mobile-hide" style="text-align:center; margin:10px;">
					Quantities are based on available stock info, and may be affected by order volumes.
				</div>
						
			</div>
		</div>
	</div>	


<div class="item-details">
	<div data-cms-area="item_details_banner" data-cms-area-filters="page_type"></div>

	<header class="item-details-header">
		<div id="banner-content-top" class="item-details-banner-top"></div>
	</header>
	<!-- <div class="item-details-divider-desktop"></div> -->
	<article class="item-details-content" itemscope itemtype="http://schema.org/Product">
		<meta itemprop="url" content="{{model._url}}">
		<div id="banner-details-top" class="item-details-banner-top-details"></div>

		<section class="item-details-main-content">
			<div class="item-details-content-header">
				<h1 class="item-details-content-header-title" itemprop="name">{{model.storedisplayname}}</h1>
				{{#if showReviews}}
				<div class="item-details-rating-header" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
					<div class="item-details-rating-header-rating" data-view="Global.StarRating"></div>
				</div>
				{{/if}}
				<div data-cms-area="item_info" data-cms-area-filters="path"></div>
			</div>

			<div class="item-details-divider"></div>

			<div class="item-details-image-gallery-container">
				<div id="banner-image-top" class="content-banner banner-image-top"></div>
				<div data-view="ItemDetails.ImageGallery"></div>
				<div id="banner-image-bottom" class="content-banner banner-image-bottom"></div>
			</div>

			<div class="item-details-divider"></div>

			<div class="item-details-main">

				<section class="item-details-info">
					<div id="banner-summary-bottom" class="item-details-banner-summary-bottom"></div>
					<div class="item-details-price">
					{{#unless model.dontshowprice}}	<div data-view="Item.Price" style="float:left"></div> {{/unless}}
					{{#if  model.dontshowprice}} <div>{{model.nopricemessage}}</div>{{/if}}
					</div>
					<br>
					<div class="item-details-sku-container">
						<span class="item-details-sku">
							{{translate 'SKU: #'}}
						</span>
						<span class="item-details-sku-value" itemprop="sku">
							{{sku}}
						</span>
					</div>
					<div data-view="Item.Stock"></div>
				</section>

				{{#if showRequiredReference}}
					<div class="item-details-text-required-reference-container">
						<small>Required <span class="item-details-text-required-reference">*</span></small>
					</div>
				{{/if}}

				<section class="item-details-options">
					{{#if isItemProperlyConfigured}}
						{{#if hasAvailableOptions}}
							<button class="item-details-options-pusher" data-type="sc-pusher" data-target="item-details-options"> {{#if isReadyForCart}}{{ translate 'Options' }}{{else}}{{ translate 'Select options' }}{{/if}} {{#if hasSelectedOptions}}:{{/if}} <i></i>
								{{#each selectedOptions}}
									{{#if @first}}
										<span> {{ label }} </span>
									{{else}}
										<span> , {{ label }} </span>
									{{/if}}
								{{/each}}
							</button>
						{{/if}}

						<div class="item-details-options-content" data-action="pushable" data-id="item-details-options">

							<div class="item-details-options-content-price" data-view="Item.Price"></div>

							<div class="item-details-options-content-stock"  data-view="Item.Stock"></div>

							<div data-view="ItemDetails.Options"></div>
						</div>
					{{else}}
						<div class="alert alert-error">
							{{translate '<b>Warning</b>: This item is not properly configured, please contact your administrator.'}}
						</div>

					{{/if}}
				</section>

				{{#if isItemProperlyConfigured}}
					<section class="item-details-actions">

					{{#unless model.dontshowprice}}	<form action="#" class="item-details-add-to-cart-form" data-validation="control-group">
							{{#if showQuantity}}
								<input type="hidden" name="quantity" id="quantity" value="1">
							{{else}}
								<div class="item-details-options-quantity" data-validation="control">
									<label for="quantity" class="item-details-options-quantity-title">
										{{translate 'Quantity'}}
									</label>

									<button class="item-details-quantity-remove" data-action="minus" {{#if isMinusButtonDisabled}}disabled{{/if}}>-</button>
									<input type="number" name="quantity" id="quantity" class="item-details-quantity-value" value="{{quantity}}" min="1">
									<button class="item-details-quantity-add" data-action="plus">+</button>

									{{#if showMinimumQuantity}}
										<small class="item-details-options-quantity-title-help">
											{{translate '(Minimum of $(0) required)' minQuantity}}
										</small>
									{{/if}}
								</div>
							{{/if}}
							
							{{#if financable}}
							<br><a target="_blank" href="{{finurl}}">Finance as low as <b>${{finamount}}/month</b>. Click to apply now! >></a>
							{{/if}}
							
							{{#if financableHigh}}
							<br><a target="_blank" href="{{finurlHigh}}">Finance as low as <b>${{finamountHigh}}/month</b>. Click to apply now! >></a>
							{{/if}}
							

							{{#unless isReadyForCart}}
								{{#if showSelectOptionMessage}}
									<p class="item-details-add-to-cart-help">
										<i class="item-details-add-to-cart-help-icon"></i>
										<span class="item-details-add-to-cart-help-text">{{translate 'Please select options before adding to cart'}}</span>
									</p>
								{{/if}}
							{{/unless}}
							<div class="item-details-actions-container">
								<div class="item-details-add-to-cart">
									<button data-type="add-to-cart" data-action="sticky" class="item-details-add-to-cart-button" {{#unless isReadyForCart}}disabled{{/unless}}>
										{{translate 'Add to Cart'}}
									</button>
								</div>
								<div class="item-details-add-to-wishlist" data-type="product-lists-control" {{#unless isReadyForWishList}} data-disabledbutton="true"{{/unless}}>
								</div>
							</div>
						</form> {{/unless}}


						<div data-type="alert-placeholder"></div>

					</section>
				{{/if}}
				<div class="item-details-main-bottom-banner">
					<div data-view="SocialSharing.Flyout"></div>
					<div class="addthis_responsive_sharing"></div>
					<div id="banner-summary-bottom" class="item-details-banner-summary-bottom"></div>
				</div>
				{{#unless model.dontshowprice}}
					{{#unless noshippingmap}}
						
						{{#unless dropship}}
							<div style="background-color: #F6F6F6; padding:25px; margin-bottom:25px;">
								<strong><a href="#" onclick="popup('popUpDiv')"> >> View Availability and Shipping Map</a></strong>
							</div>
						{{/unless}}
						
						{{#if dropship}}
							<div style="background-color: #F6F6F6; padding:25px;">
								<b>Ships in {{leadtime}} From Manufacturer</b>
							</div>
						{{/if}}
						
						{{#if flatrate}}
							<strong>Ships for {{flatrate}} in Contiguous US</strong>
						{{/if}}
						
						
						{{#if showQtyPrice}}
							<table>
								<tr><td><strong>Qty</strong></td><td><strong>Price</strong></td></tr>
								<tr><td>{{translate qtyRange}}</td><td>{{translate qtyPrice}}</td></tr>
							</table><br><br>
						{{/if}}
						
					{{/unless}}
				{{/unless}}
				
			<div id="banner-details-bottom" class="item-details-banner-details-bottom" data-cms-area="item_info_bottom" data-cms-area-filters="page_type"></div>
			</div>
		</section>

		<section class="item-details-more-info-content">
		
			{{#if showDetails}}

				<div class="item-details-more-info-content-container">

					<div id="banner-content-top" class="content-banner banner-content-top"></div>

					<div role="tabpanel">
						
						{{!-- Tab Contents --}}
						<div class="item-details-tab-content" >
						
						{{#if hasPlusItem}} 						
							<div id="item-details-content-container" style="position:relative;overflow:hidden">
							<div class="row" style="width:100%; overflow:hidden">
								<div class = "col-md-6">
								<img src="/site/img/main/plus-header.png"> <strong style="vertical-align:top">&nbsp;&nbsp;Recommended Item</strong><br>
								<h3>Check Out Ryonet Plus Products!<br>
									<a href="{{plusItem.url}}"><img src="{{plusItem.image}}" style="width:auto"></a><br>
									<h4>{{plusItem.sku}}</h4><br>
									<a href="{{plusItem.url}}">{{plusItem.name}}</a><br><br>
									{{plusItem.price}}<br>
									<a href="{{plusItem.url}}" class="cart-confirmation-modal-view-cart-button">More Info</a><br><br>
									</div>
									
									<div class="col-md-6" style="float:left"> 
										<h2>What are Ryonet Plus Products?</h2>
										<h2><ul>
											<li>Get it Faster</li>
											<li>Get Rewarded</li>
										</ul></h2><br>
										We have put all of our top selling products in one place, made them 1-2 days faster for delivery. Join our <a href="/ryonet-plus-membership-yearly">Ryonet Membership</a> today and start earning up to 10% back in member rewards for buying Plus Products.<br><br>
										<strong><a href = "/cat/ryonet-plus-products">View all Plus Products</a></strong><br><br>
									</div>
									<div class="col-md-6" style="float:left"><h2>Plus Product Shipping Map</h2>
									<img src="/img/maps/plus-ship-from-350px.png" alt="Plus Shipping Map"></div>
								</div>
							</div>
							<hr>
						{{/if}}
						
							{{#each details}}
								<br><h2>{{name}}</h2>
									<div id="item-details-content-container" {{#if setwidth}}class="video-width"{{/if}}>{{{content}}}</div>
									{{#if detailsec}} {{#if hasH2}}<br><h2>Compare to: {{h2}}</h2>{{/if}} {{/if}}
									{{#if addFiles}}
										<div><br><b>Associated Documents:</b><br>
										{{#if file1}}<a href="{{f1}}" target="_blank"><img src="/img/PDF-icon.png">	&nbsp;<b>{{file1title}}</b></a><br>{{/if}}
										{{#if file2}}<a href="{{f2}}" target="_blank"><img src="/img/PDF-icon.png">	&nbsp;<b>{{file2title}}</b></a><br>{{/if}}
										{{#if file3}}<a href="{{f3}}" target="_blank"><img src="/img/PDF-icon.png">	&nbsp;<b>{{file3title}}</b></a><br>{{/if}}
									{{/if}}
									{{#unless detailsec}}<hr>{{/unless}}
								
							{{/each}}
						</div>
					</div>
					<div id="banner-content-bottom" class="content-banner banner-content-bottom"></div>
				</div>
			{{/if}}
		</section>

		<div class="item-details-divider-desktop"></div>

		 <!-- <div class="item-details-content-related-items">
			<div data-view="Related.Items"></div>
		</div> -->
		{{#unless noCorrelated}}
		<div class="item-details-content-correlated-items">
			<div data-view="Correlated.Items"></div>
		</div>


		<div class="item-details-divider-desktop"></div>
		{{/unless}}

		<section class="item-details-product-review-content" >
			{{#if showReviews}}
				<button class="item-details-product-review-pusher" data-target="item-details-review" data-type="sc-pusher">{{ translate 'Reviews' }}
					<div class="item-details-product-review-pusher-rating" data-view="Global.StarRating"></div><i></i>
				</button>
				<div class="item-details-more-info-content-container" data-action="pushable" data-id="item-details-review">
					<div data-view="ProductReviews.Center"></div>
				</div>
			{{/if}}
		</section>


		<div id="banner-details-bottom" class="content-banner banner-details-bottom" data-cms-area="item_details_banner_bottom" data-cms-area-filters="page_type"></div>
	</article>
</div>


<!-- SCRIPT FOR SHIPPING MAP POPUP -->

<script>

function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == 'none' ) {	el.style.display = 'block';}
	else {el.style.display = 'none';}
}
function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}
	var blanket = document.getElementById('blanket');
	blanket.style.height = blanket_height + 'px';
}
function window_pos(popUpDivVar) {

	var ycoord = window.pageYOffset;
	var xcoord = window.pageXOffset;
	var winheight = window.innerHeight;
	var winwidth = window.innerWidth;
	var popUpDiv = document.getElementById('popUpDiv');
	var divwidth = 380;
	var divheight = 575;
	var newdivwidth = divwidth;
	var newdivheight = divheight;
	var divleft = ((parseInt(winwidth) - parseInt(divwidth))/2) + xcoord;
	var divtop = (parseInt(winheight) - parseInt(divheight))/2 + ycoord;
	
	if((parseInt(winwidth) - 20) < divwidth) {
	
	newdivwidth = parseInt( winwidth ) - 20;
	var ratio = divheight / divwidth;
	newdivheight = newdivwidth * ratio;
	divleft = parseInt(xcoord) + 10;
	divtop = parseInt(ycoord) + 10;
	
	}
	
	document.getElementById('popUpDiv').style.top = divtop + 'px';
	document.getElementById('popUpDiv').style.left = divleft + 'px';
	document.getElementById('popUpDiv').style.height = newdivheight + 'px';
	document.getElementById('popUpDiv').style.width = newdivwidth + 'px';
	
}

function popup(windowname) {
	blanket_size(windowname);
	window_pos(windowname);
	toggle('blanket');
	toggle(windowname);		
}


</script>


{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="order-wizard-cart-summary-container">
		<h3 class="order-wizard-cart-summary-title">
			{{translate 'Order Summary'}}
		</h3>

	<div class="order-wizard-cart-summary-body">
		{{#if showEditCartMST}}
		<div class="order-wizard-cart-summary-edit-cart-label-mst">
			<a href="#" class="order-wizard-cart-summary-edit-cart-link" data-touchpoint="viewcart">
				{{translate 'Edit Cart'}}
			</a>
		</div>
		{{/if}}
		<div class="order-wizard-cart-summary-subtotal">
			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-grid-right" >
					{{model.summary.subtotal_formatted}}
				</span>
				<span class="order-wizard-cart-summary-subtotal-label">
					{{#if itemCountGreaterThan1}}
						{{translate 'Subtotal (<span data-type="cart-summary-subtotal-count">$(0)</span> items)' itemCount}}
					{{else}}
						{{translate 'Subtotal (<span data-type="cart-summary-subtotal-count">$(0)</span> item)' itemCount}}
					{{/if}}
				</span>
			</p>
		</div>


		{{#if showPromocode}}
			<div class="order-wizard-cart-summary-promocode-applied">
				<p class="order-wizard-cart-summary-promo-code-applied">
					<strong class="order-wizard-cart-summary-discount-rate">
						{{model.summary.discountrate_formatted}}
					</strong>
					{{translate 'Promo Code Applied'}}
				</p>
				<p class="order-wizard-cart-summary-grid-float">
					#{{model.promocode.code}} - {{translate 'Instant Rebate'}}
					{{#if showRemovePromocodeButton}}
						<a href="#" data-action="remove-promocode">
							<span class="order-wizard-cart-summary-remove-container">
								<i class="order-wizard-cart-summary-remove-icon"></i>
							</span>
						</a>
					{{/if}}
				</p>
			</div>
		{{/if}}

		{{#if showDiscount}}
			<div class="order-wizard-cart-summary-discount-applied">
				<p class="order-wizard-cart-summary-grid-float">
					<span class="order-wizard-cart-summary-discount-total">
						{{model.summary.discounttotal_formatted}}
					</span>
					{{translate 'Discount Total'}}
				</p>
			</div>
		{{/if}}

		{{#if showGiftCertificates}}
			<div class="order-wizard-cart-summary-giftcertificate-applied">
				<h5 class="order-wizard-cart-summary-giftcertificate-title">{{translate 'Gift Certificates Applied ($(0))' giftCertificates.length}}</h5>
				<div data-view="GiftCertificates"></div>
			</div>
		{{/if}}


		<div class="order-wizard-cart-summary-shipping-cost-applied">
			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-shipping-cost-formatted">
					{{shippingcost}}
				</span>
				{{translate 'Shipping'}}
			</p>

			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-tax-total-formatted" >
					{{model.summary.taxtotal_formatted}}
				</span>
				{{translate 'Tax'}}
			</p>
		</div>

		<div class="order-wizard-cart-summary-total">
			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-grid-right" >
					{{model.summary.total_formatted}}
				</span>
				{{translate 'Total'}}
			</p>
		</div>
	</div>
</div>

{{#if showMembership}}
	<div class ="order-wizard-cart-summary-membership form">
	Your order qualifies you for a FREE year as a Ryonet Member!<br><br>
		<input id="enroll-member" type="checkbox" onclick="memberTrial()">
			<label for="enroll-member">YES! Sign me up for a FREE* year as a Ryonet Member, so I can take advantage of discounts and get rewards for my orders!<br>
			<a onCLick="memberPop()">Click here to see membership benefits</a></label><br><br>
			<i>*Membership renewal will auto-bill after the 1-year trial has ended.</i>
		</input>
	</div>
{{/if}}

{{#if showCartTerms}}
	{{#if requireTermsAndConditions}}
		<p class="order-wizard-cart-summary-order-message">
			<small>
				{{translate 'By placing your order, you are agreeing to our <a data-type="term-condition-link-summary" data-toggle="show-terms-summary" href="#">Terms & Conditions</a>'}}
			</small>
		</p>
	{{/if}}
{{/if}}

{{#if showContinueButton}}
	<div class="order-wizard-cart-summary-button-container">
		<button class="order-wizard-cart-summary-button-place-order" data-action="submit-step">
			{{continueButtonLabel}}
		</button>
	</div>
{{/if}}

{{#if showPromocodeForm}}
	<div class="order-wizard-cart-summary-promocode">
		<div class="order-wizard-cart-summary-promocode-expander-head">
			<a class="order-wizard-cart-summary-promocode-expander-head-toggle collapsed" data-toggle="collapse" data-target="#order-wizard-promocode" aria-expanded="false" aria-controls="order-wizard-promocode">
				{{translate 'Have a Promo Code?'}}
				<i class="order-wizard-cart-summary-promocode-tooltip" data-toggle="tooltip" title="{{translate '<b>Promo Code</b><br>To redeem a promo code, simply enter your information and we will apply the offer to your purchase during checkout.'}}"></i>
				<i class="order-wizard-cart-summary-promocode-expander-toggle-icon"></i>
			</a>
		</div>
		<div class="order-wizard-cart-summary-promocode-expander-body collapse" id="order-wizard-promocode" data-type="promo-code-container" aria-expanded="false" data-target="#order-wizard-promocode">
			<div class="order-wizard-cart-summary-promocode-expander-container">
				{{#if isMultiShipTo}}
					<div class="order-wizard-cart-summary-promocode-unsupported-summary-warning">
						<p>
							{{translate 'Shipping to multiple addresses does not support Promo Codes.'}}
						</p>
						<p>
							{{translate 'If you want to apply one, please <a href="#" data-action="change-status-multishipto-sidebar" data-type="multishipto">ship to a single address</a>.'}}
						</p>
					</div>
				{{else}}
					<div data-view="Cart.PromocodeForm"></div>
				{{/if}}
			</div>
		</div>
	</div>
{{/if}}	

{{#if showItems}}
	<div class="order-wizard-cart-summary-accordion-head">
		<a class="order-wizard-cart-summary-accordion-head-toggle {{#unless showOpenedAccordion}}collapsed{{/unless}}" data-toggle="collapse" data-target="#checkout-products" aria-controls="checkout-products">
		{{#if itemCountGreaterThan1}}
			{{translate '$(0) Items' itemCount}}
		{{else}}
			{{translate '1 Item'}}
		{{/if}}
		
		<i class="order-wizard-cart-summary-accordion-toggle-icon"></i>
		</a>
	</div>
	<div class="order-wizard-cart-summary-accordion-body collapse {{#if showOpenedAccordion}}in{{/if}}" id="checkout-products" role="tabpanel">
		<div class="order-wizard-cart-summary-accordion-container" data-content="order-items-body">
		{{#if showEditCartButton}}
		<div class="order-wizard-cart-summary-edit-cart-label">
			<a href="#" class="order-wizard-cart-summary-edit-cart-link" data-touchpoint="viewcart">
				{{translate 'Edit Cart'}}
			</a>
		</div>
		{{/if}}
		<div class="order-wizard-cart-summary-products-scroll">
			<table class="lg2sm-first">
				<tbody data-view="Items.Collection"></tbody>
			</table>
		</div>
		</div>
	</div>
{{/if}}

<script>

//Local Storage value for membership enrollment field    
    var enrollmember = localStorage.getItem("enrollmember");
    if (enrollmember != null && document.getElementById('enroll-member')) {
       document.getElementById('enroll-member').checked=true;
    }
    
    
function memberPop(){

window.open("http://www.screenprinting.com/site/ryomember.html","_blank","width=500px,height=600px,location=no,left=50px,top=50px,toolbar=no,titlebar=no,menubar=no,resizable=no");

}

function memberTrial(){


	var url = window.location.href;
	
	if(document.getElementById('enroll-member') != null) {
		var enrollmember=document.getElementById('enroll-member').checked;
		if(enrollmember) {
		
			localStorage.setItem("enrollmember", enrollmember);    
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=32100&qty=1';
			var params = '';

		} else {
		
			localStorage.removeItem("enrollmember"); 
			var command = '/app/site/backend/additemtocart.nl?c=3518025&buyid=32100&qty=-1';
			var params = '';
		
		}
		
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
		
			if (xmlhttp.readyState==4) {
				window.top.location = url;
		    }
		    
		}
		
		xmlhttp.open("GET",command,false);
		var res = xmlhttp.send();
		location.reload();

	}

}
</script>
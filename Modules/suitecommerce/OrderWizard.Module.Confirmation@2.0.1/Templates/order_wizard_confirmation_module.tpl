{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="order-wizard-confirmation-module alert fade in">
	<h2 class="order-wizard-confirmation-module-title">{{translate 'Thank you for shopping with us!'}}</h2>
	<p class="order-wizard-confirmation-module-body" name="orderNumber">{{translate 'We are looking your order over, please check your email for your order confirmation.'}}

	</p>

	<a class="order-wizard-confirmation-module-continue" href="{{continueURL}}" {{#if touchPoint}}data-touchpoint="home"{{/if}} data-hashtag="#/">{{translate 'Continue shopping'}}</a>
</div>

<script>
localStorage.removeItem("sigreqvalue");
localStorage.removeItem("gottahaveit");
localStorage.removeItem("liftgatereqvalue");
localStorage.removeItem("icanwait");
localStorage.removeItem("shipadd1");
localStorage.removeItem("shipadd2");
localStorage.removeItem("shipcity");
localStorage.removeItem("shipstate");
localStorage.removeItem("shipphone");
localStorage.removeItem("shipcountry");
localStorage.removeItem("waship");
localStorage.removeItem("usepoints");
localStorage.removeItem("pointmismatch");
</script>


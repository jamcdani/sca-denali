{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<script>

if(document.getElementById('pointscheck') != null) {
	
	if(localStorage.getItem('usepoints') == true || localStorage.getItem('usepoints') == 'true') {
	
		document.getElementById('pointscheck').checked = true;
		
	} else {
		
		document.getElementById('pointscheck').checked = false;
	}
		
	var pointmismatch = localStorage.getItem("pointmismatch");
	
	if(pointmismatch == 'true' || pointmismatch == true || pointmismatch){
	
		var url = window.location.href;
					
		var command ='/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=multi&multi=32703,0;32703,-99999999&showcart=F';
		console.log(command);	
		var xmlhttp;
		if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {  // code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		console.log(xmlhttp);
		xmlhttp.onreadystatechange=function() {
			
			if (xmlhttp.readyState==4) {
				window.top.location = url;
			}
		
			
			xmlhttp.open("GET",command,false);
			var res = xmlhttp.send();
			console.log(res);
			location.reload();
			console.log('post reload');
		}
	
	}
	
}
	
</script>

{{#if hasPoints}}
<div class="order-wizard-cart-summary-container">
		<h3 class="order-wizard-cart-summary-title">
			{{translate 'Redeem Ryonet Member Rewards'}} - You have {{pointsAsUSD}} in rewards available!
		</h3>

	<div class="order-wizard-cart-summary-body">
	<li>
				<div id="pointsdiv">
					<input id="pointscheck" type="checkbox" style="margin-left:30px;float:left" onclick="addPoints();">
			        	<label for="pointscheck" style="float:left;padding-top:0;margin-left:10px">Redeem {{pointsAsUSD}} of your rewards for this order.</label>
			    	</input>
			         <i style="margin-top:-10px" class="order-wizard-cart-summary-promocode-tooltip" data-toggle="tooltip" title data-original-title="Member Rewards are credited for purchases of Plus Products, and are available once the order has been processed.">
					</i>
				</div>
			</li>
	</div>
</div>
{{/if}}

<div class="order-wizard-paymentmethod-giftcertificates-module">
	{{#if pageHeader}}
		<header class="order-wizard-paymentmethod-giftcertificates-module-step-header">
			<h2 class="order-wizard-paymentmethod-giftcertificates-module-section-header">
				{{pageHeader}}
			</h2>
		</header>
	{{/if}}

	<div class="order-wizard-paymentmethod-giftcertificates-module-expander-head">
		<a class="order-wizard-paymentmethod-giftcertificates-module-expander-head-toggle collapsed" data-toggle="collapse" data-target="#gift-certificate-form" aria-expanded="false" aria-controls="gift-certificate-form">
			{{#if hasGiftCertificates}}
				{{translate 'Add other Gift Certificate'}}
			{{else}}
				{{translate 'Apply a Gift Certificate'}}
			{{/if}}
			<i class="order-wizard-paymentmethod-giftcertificates-module-expander-icon"></i>
		</a>
	</div>
	<form id="gift-certificate-form" class="order-wizard-paymentmethod-giftcertificates-module-form collapse" data-action="gift-certificate-form">
		<div class="order-wizard-paymentmethod-giftcertificates-module-form-expander-container">
			<fieldset>
				
				<div class="order-wizard-paymentmethod-giftcertificates-module-form-input-container">
					<input type="text" class="order-wizard-paymentmethod-giftcertificates-module-form-input" name="code">
				</div>
				<div class="order-wizard-paymentmethod-giftcertificates-module-form-submit-container">
					<button type="submit" class="order-wizard-paymentmethod-giftcertificates-module-form-submit">
					{{translate 'Apply'}}
				</button>
				</div>

				<div data-type="alert-placeholder-gif-certificate"></div>
			</fieldset>
		</div>
	</form>

	{{#if hasGiftCertificates}}
		<table class="order-wizard-paymentmethod-giftcertificates-module-table">
			<thead class="order-wizard-paymentmethod-giftcertificates-module-table-header">
				<tr>
					<th class="order-wizard-paymentmethod-giftcertificates-module-row-number">
						<span>{{translate 'Gift Certificate number'}}</span>
					</th>
					<th class="order-wizard-paymentmethod-giftcertificates-module-row-amount">
						<span>{{translate 'Amount applied'}}</span>
					</th>
					<th class="order-wizard-paymentmethod-giftcertificates-module-row-remaining">
						<span>{{translate 'Remaining balance'}}</span>
					</th>
					<th class="order-wizard-paymentmethod-giftcertificates-module-row-actions"></th>
				</tr>
			</thead>
			<tbody class="order-wizard-paymentmethod-giftcertificates-module-table-body" data-view="GiftCertificatesRecords"></tbody>
		</table>

	{{/if}}
</div>
	
<script>	

function addPoints() {

	var url = window.location.href;

	if(document.getElementById('pointscheck') != null) {
		var pointsValue=document.getElementById('pointscheck').checked;
		if(pointsValue) {

			localStorage.setItem("usepoints", pointsValue); 
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=multi&multi=32703,{{pointsToUse}}&showcart=F';

		} else {
		
			localStorage.removeItem("usepoints"); 
			var command ='/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=multi&multi=32703,0;32703,-99999999&showcart=F';

		}
		
		var xmlhttp;
		if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}
		else {  // code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
		
			if (xmlhttp.readyState==4) {
				window.top.location = url;
		    }
		}
		
		xmlhttp.open("GET",command,false);
		var res = xmlhttp.send();
		location.reload();
	}

}

</script>
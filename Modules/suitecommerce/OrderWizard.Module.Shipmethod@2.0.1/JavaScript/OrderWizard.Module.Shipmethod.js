/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderWizard.Module.Shipmethod
define(
	'OrderWizard.Module.Shipmethod'
,	[	'Wizard.Module'
	,	'Profile.Model'
	,	'GlobalViews.Message.View'

	,	'order_wizard_shipmethod_module.tpl'

	,	'underscore'
	,	'jQuery'
	]
,	function (
		WizardModule
	,	ProfileModel
	,	GlobalViewsMessageView

	,	order_wizard_shipmethod_module_tpl

	,	_
	,	jQuery
	)
{
	'use strict';
	//@class OrderWizard.Module.Shipmethod @extends Wizard.Module
	return WizardModule.extend({

		//@property {Function} template
		template: order_wizard_shipmethod_module_tpl
		//@property {Object} events
	,	events: {
			'change [data-action="select-delivery-option"]': 'changeDeliveryOptions'
		,	'click [data-action="select-delivery-option-radio"]': 'changeDeliveryOptions'
		}
		//@property {Array} errors
	,	errors: ['ERR_CHK_SELECT_SHIPPING_METHOD','ERR_WS_INVALID_SHIPPING_METHOD']
		//@method initialize
	,	initialize: function ()
		{
			this.waitShipmethod = SC.ENVIRONMENT.CART ? !SC.ENVIRONMENT.CART.shipmethod : !(this.model && this.model.get('shipmethod'));

			this.profileModel = ProfileModel.getInstance();

			this.addresses = this.profileModel.get('addresses');

			WizardModule.prototype.initialize.apply(this, arguments);
			// So we allways have a the reload promise
			this.reloadMethodsPromise = jQuery.Deferred().resolve();
			this.wizard.model.on('ismultishiptoUpdated', _.bind(this.render, this));
		}

	,	isActive: function ()
		{
			var is_shipping_required = this.wizard.model.shippingAddressIsRequired();
			if (!is_shipping_required)
			{
				this.wizard.model.unset('shipmethod');
			}
			return is_shipping_required && !this.wizard.model.get('ismultishipto');
		}

		//@method present
	,	present: function ()
		{
			this.currentAddress = this.previousAddress = this.model.get('shipaddress');
			this.eventHandlersOn();
		}
		//@method future
	,	future: function()
		{
			this.currentAddress = this.previousAddress = this.model.get('shipaddress');
			this.eventHandlersOn();
		}
		//@method past
	,	past: function()
		{
			this.waitShipmethod = !this.model.get('shipmethod');
			this.currentAddress = this.previousAddress = this.model.get('shipaddress');
			this.eventHandlersOn();
		}
		//@method eventHandlersOn
	,	eventHandlersOn: function ()
		{
			// Removes any leftover observer
			this.eventHandlersOff();
			// Adds the observer for this step
			this.model.on('change:shipaddress', this.shipAddressChange, this);

			this.model.on('change:shipmethods', function ()
			{
				_.defer(_.bind(this.render, this));
			}, this);

			var selected_address = this.addresses.get(this.currentAddress);

			if (selected_address)
			{
				selected_address.on('sync', jQuery.proxy(this, 'reloadMethods'), this);
			}
		}
		//@method eventHandlersOff
	,	eventHandlersOff: function ()
		{
			// removes observers
			this.model.off('change:shipmethods', null, this);
			this.model.off('change:shipaddress', this.shipAddressChange, this);

			var current_address = this.addresses.get(this.currentAddress)
			,	previous_address = this.addresses.get(this.previousAddress);

			if (current_address)
			{
				current_address.off('change:country change:zip', null, this);
				current_address.off('sync');
			}

			if (previous_address && previous_address !== current_address)
			{
				previous_address.off('change:country change:zip', null, this);
			}
		}
		//@method render
	,	render: function ()
		{
			if (this.state === 'present')
			{
				if (this.model.get('shipmethod') && !this.waitShipmethod)
				{
					this.trigger('ready', true);
				}
				this._render();
			}
		}
		//@method shipAddressChange
	,	shipAddressChange: function (model, value)
		{
			// if its not null and there is a difference we reload the methods
			if (this.currentAddress !== value)
			{
				this.currentAddress = value;

				var order_address = this.model.get('addresses')
				,	previous_address = this.previousAddress && (order_address.get(this.previousAddress) || this.addresses.get(this.previousAddress))
				,	current_address = this.currentAddress && order_address.get(this.currentAddress) || this.addresses.get(this.currentAddress)
				,	changed_zip = previous_address && current_address && previous_address.get('zip') !== current_address.get('zip')
				,	changed_state = previous_address && current_address && previous_address.get('state') !== current_address.get('state')
				,	changed_country = previous_address && current_address && previous_address.get('country') !== current_address.get('country');

				// if previous address is equal to current address we compare the previous values on the model.
				if (this.previousAddress && this.currentAddress && this.previousAddress === this.currentAddress)
				{
					changed_zip = current_address.previous('zip') !== current_address.get('zip');
					changed_country = current_address.previous('country') !== current_address.get('country');
					changed_state = current_address.previous('state') !== current_address.get('state');
				}

				// reload ship methods only if there is no previous address or when change the country or zipcode
				if ((!previous_address && current_address) || changed_zip || changed_country || changed_state)
				{
					// if its selected a valid address, reload Methods
					if (this.model.get('isEstimating') || this.addresses.get(this.model.get('shipaddress')))
					{
						this.reloadMethods();
					}
				}
				else
				{
					this.render();
				}

				if (value)
				{
					this.previousAddress = value;
				}

				// if we select a new address, bind the sync method for possible address edits
				if(this.currentAddress)
				{
					var selected_address = this.addresses.get(this.currentAddress);
					if(selected_address)
					{
						selected_address.on('sync', jQuery.proxy(this, 'reloadMethods'), this);
					}

					// if there was a different previous address, remove the sync handler
					if(this.previousAddress && this.previousAddress !== this.currentAddress)
					{
						var previous_selected_address = this.addresses.get(this.previousAddress);
						if(previous_selected_address)
						{
							previous_selected_address.off('sync');
						}
					}
				}
			}
		}
		//@method reloadMethods
	,	reloadMethods: function ()
		{
			// to reload the shipping methods we just save the order
			var self = this
			,	$container = this.$el;

			$container.addClass('loading');

			// Abort the previous ajax call
			this.reloadMethodsPromise.abort && this.reloadMethodsPromise.abort();
			this.reloadingMethods = true;
			this.render();
			this.reloadMethodsPromise = this.model.save(null, {
				parse: false
			,	success: function (model, attributes)
				{
					model.set({
							shipmethods: attributes.shipmethods
						,	summary: attributes.summary
					});
				}
			}).always(function (xhr)
			{
				// .always() method is excecuted even if the ajax call was aborted
				if (xhr.statusText !== 'abort')
				{
					$container.removeClass('loading');
					self.render();
					self.step.enableNavButtons();
					self.reloadingMethods = false;
				}
			});

			if (this.reloadMethodsPromise.state() === 'pending')
			{
				self.step.disableNavButtons();
			}
		}
		//@method submit
	,	submit: function ()
		{
			return this.isValid();
		}
		//@method isValid
	,	isValid: function ()
		{
			var model = this.model
			,	valid_promise = jQuery.Deferred();

			this.reloadMethodsPromise.always(function ()
			{
				if (model.get('shipmethod') && model.get('shipmethods').get(model.get('shipmethod')))
				{
					valid_promise.resolve();
				}
				else
				{
					valid_promise.reject({
						errorCode: 'ERR_CHK_SELECT_SHIPPING_METHOD'
					,	errorMessage: _('Please select a shipping method').translate()
					});
				}
			});

			return valid_promise;
		}
		//@method changeDeliveryOptions
	,	changeDeliveryOptions: function (e)
		{
			var self = this
			,	target = jQuery(e.currentTarget)
			,	targetValue = target.val() || target.attr('data-value');

			this.waitShipmethod = true;
			this.model.set('shipmethod', targetValue);
			this.step.disableNavButtons();
			this.model.save().always(function()
			{
				self.clearError();
				self.step.enableNavButtons();
			});
		}

		//@method showError render the error message
	,	showError: function ()
		{

			var global_view_message = new GlobalViewsMessageView({
					message: this.error.errorMessage
				,	type: 'error'
				,	closable: true
			});

			// Note: in special situations (like in payment-selector), there are modules inside modules, so we have several place holders, so we only want to show the error in the first place holder.
			this.$('[data-type="alert-placeholder-module"]:first').html(
				global_view_message.render().$el.html()
			);

			this.error = null;

		}
		//@method getContext @returns OrderWizard.Module.Shipmethod.Context
	,	getContext: function ()
		{
		
		this.currentAddress = this.previousAddress = this.model.get('shipaddress');

		var today = new Date();
		var startDay = 30;
		var startMonth = 11;
		var startYear = 2015;
		var endDay = 2;
		var endMonth = 12;
		var endYear = 2015;
		var withinDate = false;
		
		var profile = this.profileModel;
		var isMember = profile.get('isMember');
		
		console.log('ship seelctor model');
		console.log(this.model);
		
		var endDate = new Date(endYear, endMonth-1, endDay + 1);
		var startDate = new Date(startYear, startMonth - 1, startDay);
		
		if(startDate < today && endDate > today){
			
			withinDate = true;
			
		}
		
		var lines = this.model.get('lines').models;
			var shipAddress = this.wizard.model.get('addresses').models[0];
			var shipCountry = null;
			var shipState = shipAddress.get('state');
			var canada = false;
			var us = false;
			var icanwait = false;
			var gottahaveit = false;
			var liftgate = false;
			switch(shipAddress.get('country')){
			
			case 'CA': 
				shipCountry = 'Canada';
				canada = true;
				break;
			case 'US':
				shipCountry = 'United States';
				us = true;
				break;
			case 'PR':
				shipCountry = 'Puerto Rico';
				break;
			case 'AS':
				shipCountry= 'American Samoa';
				break;
			}
			
			var shipZip = shipAddress.get('zip');
			var orderWeight = 0;
			var itemCount = 0;
			var setcanwait = false;
			var setgotta = false;
			var setliftgate = false;
			var setsignature = false;
			var waStates = ['AK', 'AB', 'AS', 'AZ', 'BC', 'CA', 'GU', 'HI', 'ID', 'MP', 'MT', 'NV', 'NT', 'OR', 'PR', 'SK', 'UT', 'VI', 'WA', 'YT'];
			var ksStates = ['AL','CO','IL','IN','IA','KS','KY','LA','MI','MN','MS','MO','NE','NM','ND','NU','OK','OH','SC','SD','TN','TX','VA','WI','WY'];
			var shipsWA = false;
			var shipsKS = false;
			var highestTier = 1;
			var noship = true;
			var noair = false;
			
			for(var w = 0;w<waStates.length;w++){
				
				if(shipState == waStates[w]) { 
				
					shipsWA = true;
				}
				
			}
			
			for(var k = 0;k<ksStates.length;k++){
				
				if(shipState == ksStates[k]) { 
					shipsKS = true; 
				}
				
			}

			for(var l = 0;l<lines.length;l++){
				
				var nonPhys = lines[l].get('item').get('custitem_ha_nonphysicalinventory');
				var ormd = lines[l].get('item').get('custitem_orm_d_material');
				if(ormd == true){

					noair = true;
					
					
				}
				
				if(nonPhys == false){
					
					noship = false;
					
				}
				
				var itemTier = lines[l].get('item').get('custitem_tier');
				switch(itemTier) {
				
				case 'Tier 1':
					if(highestTier < 1) { highestTier = 1; }
					break;
				
				case 'Tier 2':
					if(highestTier < 2) { highestTier = 2; }
					break;
					
				case null:
					break;
					
				default:
					if(highestTier < 3) { highestTier = 3; }
					break;
				
				}
				var itemLead = lines[l].get('item').get('custitem_ryo_fulfillmentleadtime');
				var itemWeight = lines[l].get('item').get('weight');
				var itemId = lines[l].get('item').get('internalid');
				if(itemLead == 'Same Day'){ gottahaveit = true; }
				
				if(itemWeight != null) {
					
					orderWeight = parseFloat(orderWeight) + (parseFloat(itemWeight) * parseFloat(lines[l].get('quantity')));
				
				}
				
				if( itemId == 17228) { 
					
					setsignature = true; 
					localStorage.setItem("signreqvalue", true); 
					
				} 
				
				if( itemId == 17229) { 
					
					localStorage.setItem("liftgatereqvalue", true); 
					setliftgate = true; 
					
				} 
				
				if( itemId == 30760) { 
				
					setgotta = true;
					localStorage.setItem("gottahaveit", true);  
				
				}
						
				if(itemId == 30759) { 
				
					setcanwait = true; 
					localStorage.setItem("icanwait", true); 
				
				} 
				if(itemId == 32703){
					
					
					localStorage.setItem("userpoints", true);
					
				}
				
				if( itemId != 17228 &&itemId != 30760 && itemId != 30759 && itemId != 17229){ itemCount++; }
			
			}			
			
			if(highestTier > 1 && shipsWA == false && shipsKS == false && !withinDate) { icanwait = true; }
			if(highestTier > 2 && shipsKS == true && !withinDate) { icanwait = true; }
			if(shipsWA == true) { icanwait = false; }
			if(setcanwait == true) { icanwait = true; }
			
			
			if(!setcanwait) { localStorage.removeItem("icanwait"); }
			if(!setgotta)  { localStorage.removeItem("gottahaveit"); }
			if(!setliftgate) { localStorage.removeItem("liftgatereqvale"); }
			if(!setsignature) {localStorage.removeItem("signreqvalue"); }

			
			if(parseFloat(orderWeight) > 149.9) { liftgate = true; }

			var amtdiff = parseFloat(200) - parseFloat(this.model.get('summary').subtotal);
			var shipName = null;	
			

			var self = this
			,	show_enter_shipping_address_first = !this.model.get('isEstimating') && !this.profileModel.get('addresses').get(this.model.get('shipaddress'))
			,	shipping_methods = this.model.get('shipmethods').map(function (shipmethod)
				{
				shipName = shipmethod.get('name');
				var offerFree = false;
				var noshipitem = false;
				var airShip = false;
				var show = true;
				
				if(parseFloat(amtdiff) > 0 && parseFloat(amtdiff) < 100 && shipmethod.get('internalid') == 13921){
					
					offerFree = true;
					
				}
				
				
				if(shipmethod.get('internalid') == 921){
					
					noshipitem = true;
					
				}
				
				if(shipmethod.get('internalid') == 31999 || shipmethod.get('internalid') == 32000 || shipmethod.get('internalid') == 714){
					
					airShip = true;
					
				}
				
				if(shipmethod.get('internalid') == 13920 || shipmethod.get('internalid') == 13921){
					
					if(isMember){
						
						show = false;
					}
					
				}
				
				if(shipmethod.get('internalid') == 32704){
					
					if(!isMember){
						
						show = false;
						
					}
					
				}
				
				console.log(shipmethod);

					return {
							name: shipName
						,	rate_formatted: shipmethod.get('rate_formatted')
						,	internalid: shipmethod.get('internalid')
						,	offerFree: offerFree
						,	airShip: airShip
						,	amtdiff: _.formatCurrency(amtdiff)
						,	noshipitem: noshipitem
						,	isActive: shipmethod.get('internalid') === self.model.get('shipmethod')
						,	show: show
					};
				});

			//@class OrderWizard.Module.Shipmethod.Context
			return {
					//@property {LiveOrder.Model} model
					model: this.model
					//@property {Boolean} showEnterShippingAddressFirst
				,	showEnterShippingAddressFirst: show_enter_shipping_address_first
					//@property {Boolean} showLoadingMethods
				,	showLoadingMethods: this.reloadingMethods
					//@property {Boolean} hasShippingMethods
				,	hasShippingMethods: !!shipping_methods.length
					//@property {Boolean} display select instead of radio buttons
				,	showSelectForShippingMethod: shipping_methods.length > 5
					//@property {Array} shippingMethods
				,	shippingMethods: shipping_methods
					//@property {Boolean} showTitle
				,	showTitle: !this.options.hide_title
					//@property {Straing} title
				,	title: this.options.title || _('Delivery Method').translate()
				,	shipAddress: shipAddress
				,	shipCountry: shipCountry
				,	canada: canada
				,	icanwait: false
				,	gottahaveit: gottahaveit
				,	liftgate: liftgate
				,	us: us
				,	noship: noship
				,	noair: noair

			};
		}
	});
});

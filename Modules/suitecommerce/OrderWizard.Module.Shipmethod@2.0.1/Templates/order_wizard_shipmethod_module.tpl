{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class = "order-wizard-address-module-show-addresses-container">
	<h3 class="order-wizard-shipmethod-module-title">Ship To:</h3>
	<div class="address-details">
		<p><b>{{shipAddress.fullname}}</b></p>
		<p>{{shipAddress.addr1}}</p>
		{{#if shipAddress.addr2}}<p>{{shipAddress.addr2}}</p>{{/if}}
		<p>{{shipAddress.city}}, {{shipAddress.state}} {{shipAddress.zip}}</p>
		<p>{{shipCountry}}</p>
		<p>{{shipAddress.phone}}</p>
	</div>
</div>

<div class="order-wizard-shipmethod-module">
	{{#if showTitle}}
		<h3 class="order-wizard-shipmethod-module-title">
			{{title}}
		</h3>
	{{/if}}
	
	{{#if showEnterShippingAddressFirst}}
		<div class="order-wizard-shipmethod-module-message">
			{{translate 'Warning: Please enter a valid shipping address first'}}
		</div>
	{{else}}
		{{#if showLoadingMethods}}
			<div class="order-wizard-shipmethod-module-message">
				{{translate 'Loading...'}}
			</div>
		{{else}}
			{{#if hasShippingMethods}}
				{{#if showSelectForShippingMethod}}
					<select data-action="select-delivery-option" data-action="edit-module" class="order-wizard-shipmethod-module-option-select">
						<option>{{translate 'Select delivery option'}}</option>
						{{#each shippingMethods}}
						{{#if show}}
							<option 
							{{#if isActive}}selected{{/if}} 
							value="{{internalid}}"
							id="delivery-options-{{internalid}}">
								{{rate_formatted}} - {{name}}
							</option>
						{{/if}}
						{{/each}}
					</select>
				{{else}}
					{{#if noair}}
							{{#each shippingMethods}}
							{{#if show }}
								{{#unless airShip}}
									{{#unless noshipitem}}
										<a data-action="select-delivery-option-radio" 
										class="order-wizard-shipmethod-module-option {{#if isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
										data-value="{{internalid}}">
											<input type="radio" name="delivery-options" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox" 
											{{#if isActive}}checked{{/if}}
											value="{{internalid}}" 
											id="delivery-options-{{internalid}}" />
											
											<span class="order-wizard-shipmethod-module-option-name"> 
												<span class="order-wizard-shipmethod-module-option-price">{{rate_formatted}}
												</span>	{{name}} 
											</span> {{#if offerFree}}<div style="color:#4a7f35"><i>Add {{amtdiff}} to your order to get FREE shipping on supplies!</i></div>{{/if}}
										</a>
									{{/unless}}
								{{/unless}}
							{{/if}}
							{{/each}}
					{{else}}
						{{#if noship}}
							{{#each shippingMethods}}
							{{#if show}}
								{{#if noshipitem}}
									<a data-action="select-delivery-option-radio" 
									class="order-wizard-shipmethod-module-option {{#if isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
									data-value="{{internalid}}">
										<input type="radio" name="delivery-options" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox" 
										{{#if isActive}}checked{{/if}}
										value="{{internalid}}" 
										id="delivery-options-{{internalid}}" />
										
										<span class="order-wizard-shipmethod-module-option-name"> 
											<span class="order-wizard-shipmethod-module-option-price">{{rate_formatted}}
											</span>	{{name}} 
										</span> {{#if offerFree}}<div style="color:#4a7f35"><i>Add {{amtdiff}} to your order to get FREE shipping on supplies!</i></div>{{/if}}
									</a>
								{{/if}}
							{{/if}}
							{{/each}}
						{{else}}
							{{#each shippingMethods}}
							{{#if show}}
								{{#if noshipitem}}
								{{else}}
								<a data-action="select-delivery-option-radio" 
									class="order-wizard-shipmethod-module-option {{#if isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
									data-value="{{internalid}}">
										<input type="radio" name="delivery-options" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox" 
										{{#if isActive}}checked{{/if}}
										value="{{internalid}}" 
										id="delivery-options-{{internalid}}" />
										
										<span class="order-wizard-shipmethod-module-option-name"> 
											<span class="order-wizard-shipmethod-module-option-price">{{rate_formatted}}
											</span>	{{name}} 
										</span> {{#if offerFree}}<div style="color:#4a7f35"><i>Add {{amtdiff}} to your order to get FREE shipping on supplies!</i></div>{{/if}}
									</a>
								{{/if}}
							{{/if}}
							{{/each}}
						{{/if}}
					{{/if}}
				{{/if}}
			{{else}}
				<div class="order-wizard-shipmethod-module-message">
					{{translate 'Warning: No Shipping Methods are available for this address'}}
				</div>
			{{/if}}
		{{/if}}
	{{/if}}
</div>
{{#if us}}
	<div class="order-wizard-shipmethod-module">
		<h3 class="order-wizard-shipmethod-module-title">Additional Options</h3>
			<div id="custom-shipping-options" class="form">
			<ul>
			{{#if icanwait}}
			<li>
				<div id="icanwaitdiv">
					<input id="icanwait" type="checkbox" style="margin-left:30px;float:left" onclick="iCanWait();">
			        	<label for="icanwait" style="float:left;padding-top:0;margin-left:10px">I Can Wait - Save $5! </label>
			    	</input>
			        <i style="margin-top:-10px" class="order-wizard-cart-summary-promocode-tooltip"	data-toggle="tooltip" title data-original-title="<b>I Can Wait</b><br>Not in a hurry? Get $5 off shipping when you choose the &quot;I Can Wait&quot; option. We will consolidate your entire order and ship it to you when it is complete. (Adds 2 - 4 days to your wait time.)">
					</i>
				</div>
			</li>
			{{/if}}
			{{#if gottahaveit}}
			<li>
				<div id="gottahaveitdiv">
					<input id="gottahaveit" type="checkbox" style="margin-left:30px;float:left" onclick="gottaHaveIt();">
			        	<label for="gottahaveit" style="float:left;padding-top:0;margin-left:10px">Gotta Have It! - Add $5!</label>
			    	</input>
			         <i style="margin-top:-10px" class="order-wizard-cart-summary-promocode-tooltip" data-toggle="tooltip" title data-original-title="<b>Gotta Have It!</b><br>In a crunch? Choose the &quot;Gotta Have It&quot; option and add $5 to your order to get it shipped today, guaranteed! (When order is placed before 2pm local time.)">
					</i>
				</div>
			</li>
			{{/if}}
			<li><br>
			
				<div id="sigdiv">
					<input id="signaturerequired" type="checkbox" style="margin-left:30px;float:left" onclick="signatureRequired();">
		    	    	<label for="signaturerequired" style="float:left;padding-top:0;margin-left:10px">Signature Required - Add $4.75</label>
		    		</input>
		    	    <i style="margin-top:-10px" class="order-wizard-cart-summary-promocode-tooltip"	data-toggle="tooltip" title data-original-title="<b>Signature Required</b><br>Check this box to have the shipping carrier get someone to sign when delivering your items.">
					</i>
				</div>
			</li>
			{{#if liftgate}}
			<li>
				<div id="liftgatediv">
					<input id="liftgaterequired" type="checkbox" style="margin-left:30px;float:left" onclick="liftgateRequired();">
		        		<label for="liftgaterequired" style="float:left;padding-top:0;margin-left:10px">Lift Gate Required - Add $25</label>
		    		</input>
		        	 <i style="margin-top:-10px" class="order-wizard-cart-summary-promocode-tooltip" data-toggle="tooltip" title data-original-title="<b>Liftgate Required</b><br>Check this box if you are getting a large shipment, and don't have a loading dock for the freight truck to drop off at.">
					</i>
				</div>
			</li>
			{{/if}}
		</ul>
		</div>
	</div>
{{/if}}

<script>

function iCanWait() {
	
	var url = window.location.href;

	if(document.getElementById('icanwait') != null) {
		var iCanWaitValue=document.getElementById('icanwait').checked;
		if(iCanWaitValue) {
		
			localStorage.setItem("icanwait", iCanWaitValue);    		
			localStorage.removeItem("gottahaveit");	
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=multi&multi=30760,-1;30759,1&showcart=F';
	
		} else {
		
			localStorage.removeItem("icanwait"); 
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=30759&qty=-1';
			
		}
		
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}
		else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
		
			if (xmlhttp.readyState==4) {
				window.top.location = url;
		    }
		}
		
		xmlhttp.open("GET",command,false);
		var res = xmlhttp.send();
		location.reload();
	}
	
}



function gottaHaveIt() {

	var url = window.location.href;

	if(document.getElementById('gottahaveit') != null) {
		var gottaValue=document.getElementById('gottahaveit').checked;
		if(gottaValue) {

			localStorage.setItem("gottahaveit", gottaValue); 
			localStorage.removeItem("icanwait");
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=multi&multi=30760,1;30759,-1&showcart=F'

		} else {
		
			localStorage.removeItem("gottahaveit"); 
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=multi&multi=30760,-1'

		}
		
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}
		else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
		
			if (xmlhttp.readyState==4) {
				window.top.location = url;
		    }
		}
		
		xmlhttp.open("GET",command,false);
		var res = xmlhttp.send();
		location.reload();
	}

}




function liftgateRequired() {

	var url = window.location.href;
	
	if(document.getElementById('liftgaterequired') != null) {
		var liftgateValue=document.getElementById('liftgaterequired').checked;
		if(liftgateValue) {
		
			localStorage.setItem("liftgatereqvalue", liftgateValue);    
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=17229&qty=1&showcart=F';

		} else {
		
			localStorage.removeItem("liftgatereqvalue"); 
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=multi&multi=17228,0;17229,-1&showcart=F';
		
		}
		
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
		
			if (xmlhttp.readyState==4) {
				window.top.location = url;
		    }
		    
		}
		
		xmlhttp.open("GET",command,false);
		var res = xmlhttp.send();
		location.reload();

	}
	
}

function signatureRequired() {

	var url = window.location.href;
	
	if(document.getElementById('signaturerequired') != null) {
		var sigValue=document.getElementById('signaturerequired').checked;
		if(sigValue) {
		
			localStorage.setItem("sigreqvalue", sigValue);    
			var command = '/app/site/backend/intl/additemtocart.nl?c=3518025&buyid=17228&qty=1';
			var params = '';

		} else {
		
			localStorage.removeItem("sigreqvalue"); 
			var command = '/app/site/backend/additemtocart.nl?c=3518025&buyid=17228&qty=-1';
			var params = '';
		
		}
		
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
		
			if (xmlhttp.readyState==4) {
				window.top.location = url;
		    }
		    
		}
		
		xmlhttp.open("GET",command,false);
		var res = xmlhttp.send();
		location.reload();

	}
	
}



//Local Storage value for Signature required field
    var signData = localStorage.getItem("sigreqvalue");
    if (signData != null && document.getElementById('signaturerequired')) {
       document.getElementById('signaturerequired').checked=true;
    }
    
//Local Storage value for Expedited Shipping field
    var gottaData = localStorage.getItem("gottahaveit");
    if (gottaData != null && document.getElementById('gottahaveit')) {
       document.getElementById('gottahaveit').checked=true;
    }
    
//Local Storage value for Expedited Shipping field
    var waitData = localStorage.getItem("icanwait");
    if (waitData != null && document.getElementById('icanwait')) {
       document.getElementById('icanwait').checked=true;
    }
    
//Local Storage value for Liftgate required field    
    var liftgateData = localStorage.getItem("liftgatereqvalue");
    if (liftgateData != null && document.getElementById('liftgaterequired')) {
       document.getElementById('liftgaterequired').checked=true;
    }
</script>

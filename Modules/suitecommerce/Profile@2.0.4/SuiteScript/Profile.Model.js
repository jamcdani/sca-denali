/*
	Â© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/* global customer */
// Profile.js
// ----------------
// This file define the functions to be used on profile service
define(
	'Profile.Model'
,	['SC.Model', 'Utils']
,	function (SCModel, Utils)
{
	'use strict';

	return SCModel.extend({
		name: 'Profile'

	,	validation: {
			firstname: {required: true, msg: 'First Name is required'}

		// This code is commented temporally, because of the inconsistences between Checkout and My Account regarding the require data from profile information (Checkout can miss last name)
		,	lastname: {required: true, msg: 'Last Name is required'}

		,	email: {required: true, pattern: 'email', msg: 'Email is required'}
		,	confirm_email: {equalTo: 'email', msg: 'Emails must match'}
		}

	,	isSecure: request.getURL().indexOf('https') === 0

	,	get: function ()
		{
			var profile = {};

			//Only can you get the profile information if you are logged in.
			if (session.isLoggedIn() && this.isSecure)
			{

				//Define the fields to be returned
				this.fields = this.fields || ['isperson', 'email', 'image', 'internalid', 'name', 'overduebalance', 'phoneinfo', 'companyname', 'firstname', 'lastname', 'middlename', 'emailsubscribe', 'campaignsubscriptions', 'paymentterms', 'creditlimit', 'balance', 'creditholdoverride', 'custentity_ryo_membercheckbox','custentity_ryo_freemember'];

				profile = customer.getFieldValues(this.fields);
				var customFields = customer.getCustomFieldValues();
				var custField = null;
				var isMember = false;
				var isRewards = false;
				var totalPoints = 0;
				var reppic = null;
				var repemail = null;
				var custpic = null;
				var repphone = null;
				var repname = null;
				var custPoints= null;
				
				var rewardMsg = 'You have not yet enrolled as a Ryonet Member.';
				
				for(var c = 0; c<customFields.length; c++){
					
					if(customFields[c].name == 'custentity_ryo_membercheckbox'){
						
						if(customFields[c].value == 'T'){
							
							isMember  = true;
							isRewards = true;
							rewardMsg = 'As a Ryonet Member you earn <b>10%</b> back in store credit on all <a href="http://www.screenprinting.com/category/ryonet-plus-products">Plus Products</a> purchased online! Enjoy watching your credits build, and use them to buy more great things.';
							
						}
						
					}
					
					if(customFields[c].name == 'custentity_ryo_reward_points'){					
						
						custPoints  = customFields[c].value;
						
					}
					
					if(customFields[c].name == 'custentity_ryo_ryoplusandfreetotalpoints'){
						
						totalPoints = customFields[c].value;
						if(totalPoints == null || totalPoints == ''){ totalPoints = 0;}
						
					}
					
					if(customFields[c].name == 'custentity_customer_pic_url' ){
						
						custpic = customFields[c].value;
					}
					
					if(customFields[c].name == 'custentity_sales_rep_pic_url'){
						
						reppic = customFields[c].value;
					}
				
					if(customFields[c].name == 'custentity_sales_rep_email'){
						
						repemail = customFields[c].value;
					}
					
					if(customFields[c].name == 'custentity_sales_rep_name'){
						repname = customFields[c].value;
					}
					if(customFields[c].name == 'custentity_sales_rep_phone'){
						repphone = customFields[c].value;
						
					}
				}
				
				//Make some attributes more friendly to the response
				profile.custField = customFields;
				profile.isMember = isMember;
				profile.isRewards = isRewards;
				profile.rewardMsg = rewardMsg;
				profile.reppic = reppic;
				profile.custpic = custpic;
				profile.repemail = repemail;
				profile.repname = repname;
				profile.repphone = repphone;
				profile.totalPoints = custPoints;
				profile.custPoints = custPoints;
				profile.phone = profile.phoneinfo.phone;
				profile.altphone = profile.phoneinfo.altphone;
				profile.fax = profile.phoneinfo.fax;
				profile.priceLevel = (session.getShopperPriceLevel().internalid) ? session.getShopperPriceLevel().internalid : session.getSiteSettings(['defaultpricelevel']).defaultpricelevel;
				profile.type = profile.isperson ? 'INDIVIDUAL' : 'COMPANY';
				profile.isGuest = session.getCustomer().isGuest() ? 'T' : 'F';

				profile.creditlimit = parseFloat(profile.creditlimit || 0);
				profile.creditlimit_formatted = Utils.formatCurrency(profile.creditlimit);

				profile.balance = parseFloat(profile.balance || 0);
				profile.balance_formatted = Utils.formatCurrency(profile.balance);

				profile.balance_available = profile.creditlimit - profile.balance;
				profile.balance_available_formatted = Utils.formatCurrency(profile.balance_available);
			}
			else
			{
				profile = customer.getFieldValues();
				profile.subsidiary = session.getShopperSubsidiary();
				profile.language = session.getShopperLanguageLocale();
				profile.currency = session.getShopperCurrency();
				profile.isLoggedIn = Utils.isLoggedIn() ? 'T' : 'F';
				profile.isGuest = session.getCustomer().isGuest() ? 'T' : 'F';
				profile.priceLevel = session.getShopperPriceLevel().internalid ? session.getShopperPriceLevel().internalid : session.getSiteSettings('defaultpricelevel');

				profile.internalid = nlapiGetUser() + '';
			}

			return profile;
		}

	,	update: function (data)
		{
			var login = nlapiGetLogin();

			if (data.current_password && data.password && data.password === data.confirm_password)
			{
				//Updating password
				return login.changePassword(data.current_password, data.password);
			}

			this.currentSettings = customer.getFieldValues();

			//Define the customer to be updated

			var customerUpdate = {
				internalid: parseInt(nlapiGetUser(), 10)
			};

			//Assign the values to the customer to be updated

			customerUpdate.firstname = data.firstname;

			if(data.lastname !== '')
			{
				customerUpdate.lastname = data.lastname;
			}

			if(this.currentSettings.lastname === data.lastname)
			{
				delete this.validation.lastname;
			}

			customerUpdate.companyname = data.companyname;


			customerUpdate.phoneinfo = {
					altphone: data.altphone
				,	phone: data.phone
				,	fax: data.fax
			};

			if(data.phone !== '')
			{
				customerUpdate.phone = data.phone;
			}

			if(this.currentSettings.phone === data.phone)
			{
				delete this.validation.phone;
			}

			customerUpdate.emailsubscribe = (data.emailsubscribe && data.emailsubscribe !== 'F') ? 'T' : 'F';

			if (!(this.currentSettings.companyname === '' || this.currentSettings.isperson || session.getSiteSettings(['registration']).registration.companyfieldmandatory !== 'T'))
			{
				this.validation.companyname = {required: true, msg: 'Company Name is required'};
			}

			if (!this.currentSettings.isperson)
			{
				delete this.validation.firstname;
				delete this.validation.lastname;
			}

			//Updating customer data
			if (data.email && data.email !== this.currentSettings.email && data.email === data.confirm_email)
			{
				if(data.isGuest === 'T')
				{
					customerUpdate.email = data.email;
				}
				else
				{
					login.changeEmail(data.current_password, data.email, true);
				}
			}

			// Patch to make the updateProfile call work when the user is not updating the email
			data.confirm_email = data.email;

			this.validate(data);
			// check if this throws error
			customer.updateProfile(customerUpdate);

			if (data.campaignsubscriptions)
			{
				customer.updateCampaignSubscriptions(data.campaignsubscriptions);
			}

			return this.get();
		}
	});
});
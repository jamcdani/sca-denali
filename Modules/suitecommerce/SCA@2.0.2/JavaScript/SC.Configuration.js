/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module SC
// @class SC.Configuration
// All of the applications configurable defaults

define(
	'SC.Configuration'
,	[

		'item_views_option_tile.tpl'
	,	'item_views_option_text.tpl'
	,	'item_views_option_dropdown.tpl'
	,	'item_views_option_none.tpl'
	,	'item_views_selected_option.tpl'

	,	'underscore'
	,	'Utils'
	]

,	function (

		item_views_option_tile_tpl
	,	item_views_option_text_tpl
	,	item_views_option_dropdown_tpl
	,	item_views_option_none_tpl
	,	item_views_selected_option_tpl

	,	_
	)
{

	'use strict';
	
	var navigationDummyCategories = [
	               		{
	               			text: _('No Cateogry - 2').translate()
	               		,	href: '/search'
	               		,	'class': 'header-menu-level3-anchor'
	               		,	data: {
	               				touchpoint: 'home'
	               			,	hashtag: '#search'
	               			}
	               		},
	               		{
	               			text: _('No Category - 3').translate()
	               		,	href: '/search'
	               		,	'class': 'header-menu-level3-anchor'
	               		,	data: {
	               				touchpoint: 'home'
	               			,	hashtag: '#search'
	               			}
	               		},
	               		{
	               			text: _('No Category - 4').translate()
	               		,	href: '/search'
	               		,	'class': 'header-menu-level3-anchor'
	               		,	data: {
	               				touchpoint: 'home'
	               			,	hashtag: '#search'
	               			}
	               		},
	               		{
	               			text: _('No Category - 5').translate()
	               		,	href: '/search'
	               		,	'class': 'header-menu-level3-anchor'
	               		}
	               	];

	var suppliescolumn1 = [
						{
							text: _('Plus Products').translate()
						,	href: '/cat/ryonet-plus-products'
						,	'class': 'header-menu-level3-anchor-bold'
						},
						{
							text: _('Eco-Friendly').translate()
						,	href: '/cat/eco-friendly-products'
						,	'class': 'header-menu-level3-anchor-bold'
						},
						{
							text: _('New Products').translate()
						,	href: '/cat/new-screen-printing-products?order=custitem_ryo_displayinweb_date:desc'
						,	'class': 'header-menu-level3-anchor-bold'
						},
						{
							text: _('On Sale').translate()
						,	href: '/cat/on-sale'
						,	'class': 'header-menu-level3-anchor-bold'
						},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
					];
	var suppliescolumn2 = [
	               		{
	               			text: _('Plastisol Inks').translate()
	               		,	href: 'cat/plastisol-inks'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		}
	               	,	{
	               			text: _('Athletic & Polyester Inks').translate()
	               		,	href: '/cat/athletic-and-polyester-screen-printing-inks'
	               		,	'class': 'header-menu-level3-anchor'
	               		}
	               	,	{
	               			text: _('Pantone & Mixing Inks').translate()
	               		,	href: '/cat/mixing-and-pantone-inks'
	               		,	'class': 'header-menu-level3-anchor'
	               		}
	               	,	{
	               			text: _('Process Inks').translate()
	               		,	href: '/cat/process-inks'
	               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile:	true
	               		}
	               	,	{
	               			text: _('RyOpaque Inks').translate()
	               		,	href: '/cat/ryopaque-inks'
	               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile: true
	               		}
	               	,	{
               			text: _('Special Effects Inks').translate()
               		,	href: '/cat/special-effects-inks'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile:	true
               		}
	               	,	{
               			text: _('White Inks').translate()
               		,	href: '/cat/white-screen-print-inks'
               		,	'class': 'header-menu-level3-anchor no-mobile'
               		,	nomobile: true
               		}	               	,	{
               			text: _('').translate()
                   		,	href: ''
                   		,	'class': 'header-menu-level3-anchor no-mobile header-no-border'
    	               		,	nomobile:	true
                   		}
	               	,	{
               			text: _('Water Based Inks').translate()
               		,	href: '/cat/water-based-inks'
               		,	'class': 'header-menu-level3-anchor-bold'
               		}
	            	,	{
               			text: _('Allure Galaxy Inks').translate()
               		,	href: '/cat/allure-galaxy'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile: true
               		}
	               	,	{
               			text: _('Discharge Inks').translate()
               		,	href: '/cat/discharge-inks'
               		,	'class': 'header-menu-level3-anchor'
               		}
	               	,	{
               			text: _('Green Galaxy Inks').translate()
               		,	href: '/cat/green-galaxy-water-based-inks'
               		,	'class': 'header-menu-level3-anchor'
               		}
	               	,	{
               			text: _('WB Fusion Mixing System').translate()
               		,	href: '/cat/green-galaxy-fusion'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile: true
               		}
	               	,	{
               			text: _('WB Pantone & Mixing Inks').translate()
               		,	href: '/cat/water-based-mixing-and-pantone-inks'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile:	true
               		}
	               	,	{
               			text: _('').translate()
               		,	href: ''
               		,	'class': 'header-menu-level3-anchor no-mobile header-no-border'
               		,	nomobile: true
               		}
	               	,	{
               			text: _('Screens & Frames').translate()
               		,	href: '/cat/screen-printing-screens-and-frames'
               		,	'class': 'header-menu-level3-anchor-bold'
               		}
	               	,	{
               			text: _('Aluminum Screens').translate()
               		,	href: '/cat/aluminum-screen-printing-frames'
               		,	'class': 'header-menu-level3-anchor'
               		}
	               	,	{
               			text: _('Scoop Coater').translate()
               		,	href: '/cat/emulsion-scoop-coaters'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile:	true
               		}
	               	,	{
               			text: _('Solid-Loc').translate()
               		,	href: '/cat/solid-loc-stretching-system'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile:	true
               		}
	               	,	{
               			text: _('Wood Screens').translate()
               		,	href: '/cat/wood-screen-printing-frames'
               		,	'class': 'header-menu-level3-anchor'
               		}
	               	,	{
               			text: _('Squeegees').translate()
               		,	href: '/cat/silk-screening-squeegees'
               		,	'class': 'header-menu-level3-anchor-bold'
               		}
	               	,	{
               			text: _('Blade Only').translate()
               		,	href: '/cat/squeegee-blades'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile:	true
               		},
               		{
               			text: _('').translate()
               		,	href: ''
               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border' 
	               		,	mobileonly: true
               		},
               		{
               			text: _('Contact Us').translate()
               		,	href: '/contact-us-1'
               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
	               		,	mobileonly: true
               		}
	               	];
	var suppliescolumn3 = [
	               		{
	               			text: _('Mesh').translate()
	               		,	href: '/cat/screen-printing-mesh'
	               		,	'class': 'header-menu-level3-anchor-bold no-mobile'
		               		,	nomobile:	true
	               		}
	               	,	{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor no-mobile header-no-border'
		               		,	nomobile:	true
	               		}
	               	,	{
	               			text: _('Films & Output Inks').translate()
	               		,	href: '/cat/output-printing-films-and-inks'
	               		,	'class': 'header-menu-level3-anchor-bold'
               			}
	            	,	{
               			text: _('BlackMAX Inks').translate()
               		,	href: '/cat/blackmax-inks'
               		,	'class': 'header-menu-level3-anchor'
           			},	{
               			text: _('Laser Films').translate()
                   		,	href: '/cat/laser-films'
                   		,	'class': 'header-menu-level3-anchor no-mobile'
    	               		,	nomobile: true
               			}
           			,	{
               			text: _('Inkjet Waterproof Films').translate()
               		,	href: '/cat/inkjet-waterproof-films'
               		,	'class': 'header-menu-level3-anchor'
           			}
           			,	{
               			text: _('').translate()
               		,	href: ''
               		,	'class': 'header-menu-level3-anchor header-no-border'
           			}
           			,	{
               			text: _('Chemicals').translate()
               		,	href: '/cat/screen-printing-chemicals'
               		,	'class': 'header-menu-level3-anchor-bold'
           			}
           			,	{
               			text: _('Aerosols').translate()
               		,	href: '/cat/screen-printing-aerosols'
               		,	'class': 'header-menu-level3-anchor'
           			}
           			,	{
               			text: _('Cleaning Chemicals').translate()
               		,	href: '/cat/cleaning-chemicals'
               		,	'class': 'header-menu-level3-anchor'
           			}
           			,	{
               			text: _('Emulsions').translate()
               		,	href: '/cat/screen-printing-emulsion-supplies'
               		,	'class': 'header-menu-level3-anchor'
           			}
           			,	{
               			text: _('Scoop Coaters').translate()
               		,	href: '/cat/emulsion-scoop-coaters'
               		,	'class': 'header-menu-level3-anchor no-mobile'
	               		,	nomobile: true
           			}
           			,	{
               			text: _('Sgreen Chemicals').translate()
               		,	href: '/cat/sgreen-green-chemicals'
               		,	'class': 'header-menu-level3-anchor'
           			}
           			,	{
               			text: _('').translate()
               		,	href: ''
               		,	'class': 'header-menu-level3-anchor header-no-border'
           			}
           			,	{
               			text: _('Tape').translate()
               		,	href: '/cat/screen-printing-tape'
               		,	'class': 'header-menu-level3-anchor-bold'
           			},
               		{
               			text: _('').translate()
               		,	href: ''
               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
               		,	mobileonly: true
               		},
               		{
               			text: _('Contact Us').translate()
               		,	href: '/contact-us-1'
               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
                   		,	mobileonly: true
               		}
	               	];
	var suppliescolumn4 = [
	               		{
	               			text: _('Heat Transfers').translate()
	               		,	href: 'cat/heat-transfer-supplies'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Heat Press Equipment').translate()
	               		,	href: '/cat/heat-press-equipment'
	               		,	'class': 'header-menu-level3-anchor no-mobile'
		               		,	nomobile: true
	               		},
	               		{
	               			text: _('Transfer Paper').translate()
	               		,	href: '/cat/transfer-paper'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Vinyl Heat Transfers').translate()
	               		,	href: '/cat/vinyl-heat-transfers'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor no-mobile header-no-border'
		               		,	nomobile:	true
	               		},
	               		{
	               			text: _('Tools & Accessories').translate()
	               		,	href: '/cat/screen-printing-tools-and-accessories'
	               		,	'class': 'header-menu-level3-anchor-bold no-mobile'
		               		,	nomobile:	true
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor no-mobile header-no-border'
		               		,	nomobile:	true
	               		},
	               		{
	               			text: _('Software & Clipart').translate()
	               		,	href: '/cat/screen-printing-software-and-clipart'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('AccuRIP Software').translate()
	               		,	href: '/cat/accurip-rip-software'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Clipart').translate()
	               		,	href: 'cat/screen-printing-art'
	               		,	'class': 'header-menu-level3-anchor no-mobile'
		               		,	nomobile:	true
	               		},
	               		{
	               			text: _('Adobe Illustrator Software').translate()
	               		,	href: '/cat/adobe-illustrator'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('CorelDRAW Software').translate()
	               		,	href: '/cat/corel-draw-software'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Separation Software').translate()
	               		,	href: '/cat/separation-software'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
               		];
	var suppliescolumn5 = [
	               		{
	               			text: _('Shop By Brand').translate()
	               		,	href: '/cat/shop-by-supply-brand'
	               		,	'class': 'header-menu-level3-anchor-bold no-mobile'
		               		,	nomobile: true
	               		},
	               		{
	               			text: _('CCI').translate()
	               		,	href: '/cat/cci-chemicals-for-screen-printing'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Green Galaxy').translate()
	               		,	href: '/cat/green-galaxy-inks'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('IC Inks').translate()
	               		,	href: '/cat/ic-screen-printing-inks'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Rite Film').translate()
	               		,	href: '/cat/rite-film-inkjet-film-positives'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Ryonet').translate()
	               		,	href: '/cat/ryonet'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Sgreen').translate()
	               		,	href: '/cat/sgreen-green-chemicals'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Solid-Loc').translate()
	               		,	href: '/cat/solid-loc-stretching-system'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Siser').translate()
	               		,	href: '/cat/siser-easyweed-vinyl-supplies'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Sprayway').translate()
	               		,	href: '/cat/sprayway-cleaners'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Yudu').translate()
	               		,	href: '/cat/yudu-supplies'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor header-no-border'
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
	               		}
	               	];
	var equipcolumn1 = [
	               		{
	               			text: _('New Products').translate()
	               		,	href: '/cat/new-screen-printing-products?order=custitem_ryo_displayinweb_date:desc'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('On Sale').translate()
	               		,	href: '/cat/on-sale'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Used Equipment').translate()
	               		,	href: '/cat/used-equipment'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
	               	];
	var equipcolumn2 = [
	               		{
	               			text: _('Automatic Equipment').translate()
	               		,	href: '/cat/roq-automatic-machines'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('BlackMAX Output Printers').translate()
	               		,	href: '/cat/blackmax-output-printers'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Conveyor Dryers').translate()
	               		,	href: '/cat/conveyor-dryers'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Dip Tanks & Dark Room').translate()
	               		,	href: '/cat/dip-tanks-and-dark-room-supplies'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Exposure Units').translate()
	               		,	href: '/cat/exposure-units'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Flash Dryers').translate()
	               		,	href: '/cat/flash-dryers'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Heat Transfer Presses').translate()
	               		,	href: '/cat/heat-press-equipment'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Screen Racks & Carts').translate()
	               		,	href: '/cat/screen-racks-and-carts'
	               		,	'class': 'header-menu-level3-anchor-bold no-mobile'
		               		,	nomobile:	true
	               		},
	               		{
	               			text: _('Vinyl Cutters & Plotters').translate()
	               		,	href: '/cat/vinyl-cutters-and-plotters'
	               		,	'class': 'header-menu-level3-anchor-bold no-mobile'
		               		,	nomobile:	true
	               		},
	               		{
	               			text: _('Washout Booths').translate()
	               		,	href: '/cat/washout-booths'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
	               	];
	var equipcolumn3 = [
	               		{
	               			text: _('Screen Printing Presses').translate()
	               		,	href: '/cat/screen-printing-presses'
	               		,	'class': 'header-menu-level3-anchor-bold no-mobile'
	               		},
	               		{
	               			text: _('Riley Hopkins Presses').translate()
	               		,	href: '/cat/riley-hopkins-presses'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('RileyROQ Presses').translate()
	               		,	href: '/cat/riley-roq-screen-print-presses'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('ROQ Automatic Presses').translate()
	               		,	href: '/roq-automatic-screen-printing-machines-1'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('DIY Presses').translate()
	               		,	href: '/cat/diy-screen-printing-presses'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor header-no-border'
	               		},
	               		{
	               			text: _('Platens').translate()
	               		,	href: '/cat/screen-printing-platens'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Aluminum Platens').translate()
	               		,	href: '/cat/aluminum-screen-printing-platens'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Wood Platens').translate()
	               		,	href: '/cat/wood-screen-printing-platens'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
	               		,	mobileonly: true
	               		}
	               	];
	var equipcolumn4 = [
	               		{
	               			text: _('Shop By Brand').translate()
	               		,	href: '/cat/shop-by-equipment-brand'
	               		,	'class': 'header-menu-level3-anchor-bold no-mobile'
	               			, nomobile: true
	               		},
	               		{
	               			text: _('BBC Conveyer & Flash Dryers').translate()
	               		,	href: '/cat/bbc-conveyor-and-flash-dryers'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Blackline Washout Booths').translate()
	               		,	href: '/cat/blackline-washout-booths'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('CCI Chemicals').translate()
	               		,	href: '/cat/cci-chemicals-for-screen-printing'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('George Knight Heat Presses').translate()
	               		,	href: '/cat/george-knight-heat-presses'
	               		,	'class': 'header-menu-level3-anchor no-mobile'
		               		,	nomobile:	true
	               		},
	               		{
	               			text: _('Graphtec Plotters & Cutters').translate()
	               		,	href: '/cat/graphtec-plotters-and-cutters'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Riley Hopkins').translate()
	               		,	href: '/cat/riley-hopkins'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('ROQ Equipment').translate()
	               		,	href: '/cat/sroque-equipment'
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('Stahls Heat Presses').translate()
	               		,	href: '/cat/stahls-heat-presses'
		               		,	nomobile:	true
	               		,	'class': 'header-menu-level3-anchor'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor header-no-border'
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
	               		}
	               	];
	var contactuscol = [
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		}
	               	];
	var kitcolumn1 = [
	               		{
	               			text: _('Starter/DIY Kits').translate()
	               		,	href: '/cat/starter-and-diy-screen-printing-kits'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		}
	               	,	{
	               			text: _('Financing').translate()
	               		,	href: '/cat/screen-print-equipment-financing'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
	               	];
	var kitcolumn2 = [
	               		{
	               			text: _('Manual Shop Packages').translate()
	               		,	href: '/cat/manual-screen-printing-shop-packages'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
	               	];
	var kitcolumn3 = [
	               		{
	               			text: _('Financing').translate()
	               		,	href: '/cat/screen-print-equipment-financing'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
	               	];
	var educationcolumn1 = [
	               		{
	               			text: _('Class Testimonials').translate()
	               		,	href: '/screen-print-experience-testimonials-1'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('How-To Videos').translate()
	               		,	href: '/screen-printing-how-to-videos'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('Webinars & Events').translate()
	               		,	href: '/events-1'
	               		,	'class': 'header-menu-level3-anchor-bold'
	               		},
	               		{
	               			text: _('').translate()
	               		,	href: ''
	               		,	'class': 'header-menu-level3-anchor mobile-only'
		               		,	mobileonly: true
	               		},
	               		{
	               			text: _('Contact Us').translate()
	               		,	href: '/contact-us-1'
	               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
		               		,	mobileonly: true
	               		}
	               	];
	var educationcolumn2 = [
		               		{
		               			text: _('Screen Print Experience').translate()
		               		,	href: '/cat/screen-printing-classes-and-courses'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('California').translate()
		               		,	href: '/cat/california-screen-printing-classes'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Cerritos').translate()
		               		,	href: '/screen-print-experience-class-la'
		               		,	'class': 'header-menu-level3-anchor-indent'
		               		},
		               		{
		               			text: _('Florida').translate()
		               		,	href: '/screen-print-experience-class-fl'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Illinois').translate()
		               		,	href: '/screen-print-experience-class-chicago'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Louisiana').translate()
		               		,	href: '/screen-print-experience-class-sp'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('New York').translate()
		               		,	href: '/screen-print-experience-class-ny'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Ohio').translate()
		               		,	href: '/screen-print-experience-class-oh'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Texas').translate()
		               		,	href: '/screen-print-experience-class-tx'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Washington').translate()
		               		,	href: '/screen-print-experience-class-wa'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor mobile-only'
			               		,	mobileonly: true
		               		},
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
			               		,	mobileonly: true
		               		}
		               	];
	var educationcolumn3 = [
		               		{
		               			text: _('Expanded').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Louisiana').translate()
		               		,	href: '/screen-print-experience-class-expanded-sp'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Washington').translate()
		               		,	href: '/screen-print-experience-class-expanded-wa'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor mobile-only'
			               		,	mobileonly: true
		               		},
		               		{
		               			text: _('Online Courses').translate()
		               		,	href: '/cat/screen-printing-online-course'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
			               		,	mobileonly: true
		               		}
		               	];
	var educationcolumn4 = [
		               		{
		               			text: _('Workshops').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('DIY Print Workshop').translate()
		               		,	href: '/cat/diy-print-workshop'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('Water Based Printing WorkShop').translate()
		               		,	href: '/cat/water-based-green-galaxy-workshop'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor header-no-border'
		               		},
		               		{
		               			text: _('Software').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Adobe Illustrator').translate()
		               		,	href: '/cat/adobe-illustrator'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('CorelDRAW').translate()
		               		,	href: '/cat/corel-draw-software'
		               		,	'class': 'header-menu-level3-anchor'
		               		},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor mobile-only'
			               		,	mobileonly: true
		               		},
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
			               		,	mobileonly: true
		               		}
		               	];
	var educationcolumn5 = [
		               		{
		               			text: _('Educational DVDs').translate()
		               		,	href: '/cat/silk-screening-educational-dvds'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Educational Books').translate()
		               		,	href: '/cat/screen-printing-books'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor header-no-border'
		               		}
		               		,
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		}
		               	];
	var resourcecolumn1 = [
		               		{
		               			text: _('Live Chat').translate()
		               		,	href: 'http://messenger.providesupport.com/messenger/ryonet.html'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		,	'rel' : 'nofollow'
		               		},
		               		{
		               			text: _('Screen Print Library').translate()
		               		,	href: 'http://support.screenprinting.com/forums'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Help Desk').translate()
		               		,	href: 'http://support.screenprinting.com/anonymous_requests/new'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Find a Printer').translate()
		               		,	href: 'http://printers.screenprinting.com'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
			               		,	mobileonly: true
		               		},
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
			               		,	mobileonly: true
		               		}
		               	];
	var resourcecolumn2 = [
		               		{
		               			text: _('ASI Membership').translate()
		               		,	href: '/asi-membership-1'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Re-Meshing Services').translate()
		               		,	href: '/screen-re-meshing-1'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Screen Setup Services').translate()
		               		,	href: '/screen-printing-services-1'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Wholesale T-Shirts').translate()
		               		,	href: '/wholesale-t-shirts-1'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('Tax Exempt Forms').translate()
		               		,	href: '/resale-exemption-1'
		               		,	'class': 'header-menu-level3-anchor-bold'
		               		},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor mobile-only header-no-border'
			               		,	mobileonly: true
		               		},
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
			               		,	mobileonly: true
		               		}
		               	];
	var resourcecolumn3 = [
							{
								text: _('FAQs').translate()
							,	href: '/faq-1'
							,	'class': 'header-menu-level3-anchor-bold'
							},
							{
								text: _('Flash Dryer FAQ').translate()
							,	href: '/different-types-of-flash-drying-units-1'
							,	'class': 'header-menu-level3-anchor-bold'
							},
							{
								text: _('Plastisol Heat Transfer Paper FAQ').translate()
							,	href: '/plastisol-heat-transfer-paper-1'
							,	'class': 'header-menu-level3-anchor-bold'
							},
							{
								text: _('Additional Resources').translate()
							,	href: 'http://info.screenprinting.com/'
							,	'class': 'header-menu-level3-anchor-bold'
							},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor mobile-only'
			               		,	mobileonly: true
		               		},
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
			               		,	mobileonly: true
		               		}
		               	];
	var resourcecolumn4 = [
	                       {
								text: _('About Ryonet').translate()
							,	href: '/about-ryonet-1'
							,	'class': 'header-menu-level3-anchor-bold'
							},
							{
								text: _('Bio of Ryan Moor').translate()
							,	href: '/bio-of-ryan-moor-ceo-1'
							,	'class': 'header-menu-level3-anchor'
							},
							{
								text: _('Locations').translate()
							,	href: '/locations-1'
							,	'class': 'header-menu-level3-anchor'
							},
							{
								text: _('Shipping, Return & tax Policies').translate()
							,	href: '/shipping-return-policies-1'
							,	'class': 'header-menu-level3-anchor'
							},
							{
								text: _('Privacy Policy').translate()
							,	href: '/privacy-policy-1'
							,	'class': 'header-menu-level3-anchor'
							},
		               		{
		               			text: _('').translate()
		               		,	href: ''
		               		,	'class': 'header-menu-level3-anchor mobile-only'
			               		,	mobileonly: true
		               		},
		               		{
		               			text: _('Contact Us').translate()
		               		,	href: '/contact-us-1'
		               		,	'class': 'header-menu-level3-anchor-bold mobile-only'
			               		,	mobileonly: true
		               		}
		               		
		               	];


	var Configuration = {


		// @property {Object} searchPrefs Search preferences
		searchPrefs:
		{
			// @property {Number} searchPrefs.maxLength Search preferences. keyword maximum string length - user won't be able to write more than 'maxLength' chars in the search box
			maxLength: 40

			// @property {Function} searchPrefs.keywordsFormatter Search preferences. Keyword formatter function will format the text entered by the user in the search box. This default implementation will remove invalid keyword characters like *()+-="
		,	keywordsFormatter: function (keywords)
			{
				if (keywords === '||')
				{
					return '';
				}

				var anyLocationRegex = /[\(\)\[\]\{\~\}\!\"\:\/]{1}/g // characters that cannot appear at any location
				,	beginingRegex = /^[\*\-\+]{1}/g // characters that cannot appear at the begining
				,	replaceWith = ''; // replacement for invalid chars

				return keywords.replace(anyLocationRegex, replaceWith).replace(beginingRegex, replaceWith);
			}
		}

		// @property {String} imageNotAvailable url for the not available image
	,	imageNotAvailable: _.getAbsoluteUrl('img/no_image_available.jpeg')

	,	templates: {
			itemOptions: {
				// each apply to specific item option types
				selectorByType:	{
					select: item_views_option_tile_tpl
				,	'default': item_views_option_text_tpl
				}
				// for rendering selected options in the shopping cart
			,	selectedByType: {
					'default': item_views_selected_option_tpl
				}
			}
		}

		// @class SCA.Shopping.Configuration
		// @property {Array<Object>} footerNavigation links that goes in the footer
	,	footerNavigation: [
			{text: 'Link a', href:'#'}
		,	{text: 'Link b', href:'#'}
		,	{text: 'Link c', href:'#'}
		]

		// @property {closable:Boolean,saveInCookie:Boolean,anchorText:String,message:String} cookieWarningBanner
		// settings for the cookie warning message (mandatory for UK stores)
	,	cookieWarningBanner: {
			closable: true
		,	saveInCookie: true
		,	anchorText: _('Learn More').translate()
		,	message: _('To provide a better shopping experience, our website uses cookies. Continuing use of the site implies consent.').translate()
		}

		// @class SCA.Shopping.Configuration
		// @property {betweenFacetNameAndValue:String,betweenDifferentFacets:String,betweenDifferentFacetsValues:String,betweenRangeFacetsValues:String,betweenFacetsAndOptions:String,betweenOptionNameAndValue:String,betweenDifferentOptions:String}
	,	facetDelimiters: {
			betweenFacetNameAndValue: '/'
		,	betweenDifferentFacets: '/'
		,	betweenDifferentFacetsValues: ','
		,	betweenRangeFacetsValues: 'to'
		,	betweenFacetsAndOptions: '?'
		,	betweenOptionNameAndValue: '='
		,	betweenDifferentOptions: '&'
		}
		// Output example: /brand/GT/style/Race,Street?display=table

		// eg: a different set of delimiters
		/*
		,	facetDelimiters: {
				betweenFacetNameAndValue: '-'
			,	betweenDifferentFacets: '/'
			,	betweenDifferentFacetsValues: '|'
			,	betweenRangeFacetsValues: '>'
			,	betweenFacetsAndOptions: '~'
			,	betweenOptionNameAndValue: '/'
			,	betweenDifferentOptions: '/'
		}
		*/
		// Output example: brand-GT/style-Race|Street~display/table

		// @param {Object} searchApiMasterOptions options to be passed when querying the Search API
	,	searchApiMasterOptions: {

			Facets: {
				include: 'facets'
			,	fieldset: 'search'
			}

		,	itemDetails: {
				include: 'facets'
			,	fieldset: 'details'
			}

		,	relatedItems: {
				fieldset: 'details'
			}

		,	correlatedItems: {
				fieldset: 'details'
			}

			// don't remove, get extended
		,	merchandisingZone: {}

		,	typeAhead: {
				fieldset: 'typeahead'
			}
		}


		// @property {String} logoUrl header will show an image with the url you set here
	,	logoUrl: _.getAbsoluteUrl('img/screenprinting-logo.png')

		// @property {String} defaultSearchUrl
	,	defaultSearchUrl: 'search'

		// @property {Boolean} isSearchGlobal setting it to false will search in the current results
		// if on facet list page
	,	isSearchGlobal: true

		// @property {#obj(minLength: Number, maxResults: Number, macro: String, sort: String)} typeahead Typeahead Settings
	,	typeahead: {
			minLength: 2
		,	maxResults: 6
		,	macro: 'typeahead'
		,	sort: 'custitem_ryo_mostpopularsort:desc'
		}

		// @property {Array<NavigationData>} NavigationData array of links used to construct navigation. (maxi menu and sidebar)
		// @class NavigationData
	,	navigationData: [
		{
			text: _('Supplies').translate()
		,	href: '/search'
		,	'class': 'header-menu-level1-anchor'
			// @property {Array<NavigationData>} categories
		,	categories: [
				{
					text: _('Deals').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: suppliescolumn1
				}
			,	{
					text: _('Inks & Screens').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: suppliescolumn2
				}
			,	{
					text: _('Films & Chemicals').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: suppliescolumn3
				}
			,	{
					text: _('Heat Transfer & Software').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: suppliescolumn4
				}
			,	{
					text: _('Shop By Brand').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor no-mobile'
				,	nomobile: true
				,	categories: suppliescolumn5
				}
			]
		}
	,	{
			text: _('Equipment').translate()
		,	href: '/search'
		,	'class': 'header-menu-level1-anchor'
			// @property {Array<NavigationData>} categories
		,	categories: [
				{
					text: _('Deals').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: equipcolumn1
				}
			,	{
					text: _('Equipment').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: equipcolumn2
				}
			,	{
					text: _('Presses & Platens').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: equipcolumn3
				}
			,	{
					text: _('Shop By Brand').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: equipcolumn4
				}
			]
		}
	,	{
			text: _('Kit & Packages').translate()
			,	href: '/search'
			,	'class': 'header-menu-level1-anchor'
			,	categories: [
				{
					text: _('Starter/DIY Kits').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: kitcolumn1
				}
			,	{
					text: _('Intermediate/Pro Packages').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: kitcolumn2
				}
			]
        }
	,	{
			text: _('Education').translate()
		,	href: '/search'
		,	'class': 'header-menu-level1-anchor'
			// @property {Array<NavigationData>} categories
		,	categories: [
				{
					text: _('Testimonials & Video').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: educationcolumn1
				}
			,	{
					text: _('Beginner/Intermediate Classes').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: educationcolumn2
				}
			
			,	{
					text: _('Advanced Classes').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: educationcolumn3
				}
			,	{
				text: _('Work Shops & Software').translate()
			,	href: '/search'
			,	'class': 'header-menu-level2-anchor'
			,	categories: educationcolumn4
			}
			,	{
					text: _('Books & DVDs').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: educationcolumn5
				}
			]
		},	
		{
			text: _('Resources').translate()
			,	href: '/search'
			,	'class': 'header-menu-level1-anchor'
				// @property {Array<NavigationData>} categories
			,	categories: [
					{
						text: _('Help').translate()
					,	href: '/search'
					,	'class': 'header-menu-level2-anchor'
					,	categories: resourcecolumn1
					}
				,	{
						text: _('Services').translate()
					,	href: '/search'
					,	'class': 'header-menu-level2-anchor'
					,	categories: resourcecolumn2
					}
				
				,	{
						text: _('FAQs').translate()
					,	href: '/search'
					,	'class': 'header-menu-level2-anchor'
					,	categories: resourcecolumn3
					}
				,	{
					text: _('About').translate()
				,	href: '/search'
				,	'class': 'header-menu-level2-anchor'
				,	categories: resourcecolumn4
				}
				,	{
						text: _('Contact Us').translate()
					,	href: '/search'
					,	'class': 'header-menu-level2-anchor'
					,	categories: contactuscol
					}
				]
			}
		]

		// @property {Object} imageSizeMapping map of image custom image sizes
		// usefull to be customized for smaller screens
	,	imageSizeMapping: {
			thumbnail: 'thumbnail' // 175 * 175
		,	main: 'main' // 600 * 600
		,	tinythumb: 'tinythumb' // 50 * 50
		,	zoom: 'zoom' // 1200 * 1200
		,	fullscreen: 'fullscreen' // 1600 * 1600
		, homeslider: 'homeslider' // 200 * 220
		, homecell: 'homecell' // 125 * 125
		}

		// @property {Object} When showing your credit cards, which icons should we use
	,	creditCardIcons: {
			'VISA': 'img/visa.png'
		,	'Discover': 'img/discover.png'
		,	'Master Card': 'img/master.png'
		,	'Maestro': 'img/maestro.png'
		,	'American Express': 'img/american.png'
		}

	,	bxSliderDefaults: {
			minSlides: 2
		,	slideWidth: 228
		,	maxSlides: 5
		,	forceStart: true
		,	pager: false
		,	touchEnabled:true
		,	nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
		,	prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
		,	controls: true
		,	preloadImages: 'all'
		}

	,	siteSettings: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.siteSettings || {}

	,	get: function (path, defaultValue)
		{
			return _.getPathFromObject(this, path, defaultValue);
		}

	,	getRegistrationType: function ()
		{
			if (Configuration.get('siteSettings.registration.registrationmandatory') === 'T')
			{
				// no login, no register, checkout as guest only
				return 'disabled';
			}
			else
			{
				if (Configuration.get('siteSettings.registration.registrationoptional') === 'T')
				{
					// login, register, guest
					return 'optional';
				}
				else
				{
					if (Configuration.get('siteSettings.registration.registrationallowed') === 'T')
					{
						// login, register, no guest
						return 'required';
					}
					else
					{
						// login, no register, no guest
						return 'existing';
					}
				}
			}
		}
	};

	// Append Product Lists configuration
	_.extend(Configuration, {
		product_lists: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.PRODUCTLISTS_CONFIG
	});

	// Append Cases configuration
	_.extend(Configuration, {
		cases: {
			config: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.CASES_CONFIG
		,	enabled: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.casesManagementEnabled
		}
	});

	_.extend(Configuration, {
		useCMS: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.useCMS
	});
	
	// Append Bronto Integration configuration
	_.extend(Configuration, {
		bronto: {
			accountId: ''
		}
	});

	return Configuration;
});

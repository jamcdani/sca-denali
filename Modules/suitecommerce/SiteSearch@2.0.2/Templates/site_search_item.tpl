{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<a class="site-search-item-results" data-hashtag="{{model._url}}" data-touchpoint="home">
	<div class = "site-search-item-results-image">
	    <div  class="site-search-item-image-container">
	        <img data-loader="false" class="typeahead-image typeahead-main-image" src="{{resizeImage image 'thumbnail'}}" alt="{{itemid}}">
	        {{#if plus}} <img class="typeahead-overlay" src="/img/search/flag-plus.png">{{/if}}
	        {{#if isnew}} <img class="typeahead-overlay" src="/img/search/flag-new.png">{{/if}}
	        {{#if sale}} <img class="typeahead-overlay" src="/img/search/flag-sale.png">{{/if}}
	        
	    </div>
    </div>
    <div class="site-search-item-tile site-search-item-results-content">
        <div class="site-search-item-results-title">
            {{highlightKeyword model.storedisplayname query}}{{#unless dontshowprice}} - {{price}}{{/unless}}
        </div>
       <!--  <div data-view="Global.StarRating"></div> -->
    </div>
</a>